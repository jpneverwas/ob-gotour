package obtour

// To run this alone, you need to load tour.go and util.go; in bash shell:
// go test ./{tour,util,patched*}.go

import (
	"io/ioutil"
	"path/filepath"
	"reflect"
	"strings"
	"testing"
	"text/template"

	"golang.org/x/tools/present"
)

func TestSplit(t *testing.T) {
	var tests = []struct {
		in  string
		out []string
	}{
		{"", []string{}},
		{" ", []string{" "}},
		{"abc", []string{"abc"}},
		{"abc def", []string{"abc", " ", "def"}},
		{"abc def ", []string{"abc", " ", "def", " "}},
		{"hey [[http://golang.org][Gophers]] around",
			[]string{"hey", " ", "[[http://golang.org][Gophers]]", " ", "around"}},
		{"A [[http://golang.org/doc][two words]] link",
			[]string{"A", " ", "[[http://golang.org/doc][two words]]", " ", "link"}},
		{"Visit [[http://golang.org/doc]] now",
			[]string{"Visit", " ", "[[http://golang.org/doc]]", " ", "now"}},
		{"not [[http://golang.org/doc][a [[link]] ]] around",
			[]string{"not", " ", "[[http://golang.org/doc][a [[link]]", " ", "]]", " ", "around"}},
		{"[[http://golang.org][foo bar]]",
			[]string{"[[http://golang.org][foo bar]]"}},
		{"ends with [[http://golang.org][link]]",
			[]string{"ends", " ", "with", " ", "[[http://golang.org][link]]"}},
		{"my talk ([[http://talks.golang.org/][slides here]])",
			[]string{"my", " ", "talk", " ", "(", "[[http://talks.golang.org/][slides here]]", ")"}},
	}
	for _, test := range tests {
		out := split(test.in)
		if !reflect.DeepEqual(out, test.out) {
			t.Errorf("split(%q):\ngot\t%q\nwant\t%q", test.in, out, test.out)
		}
	}
}

func TestObFont(t *testing.T) {
	var tests = []struct {
		in  string
		out string
	}{
		{"", ""},
		{" ", " "},
		{"\tx", "\tx"},
		{"_a_", "/a/"},
		{"*a*", "*a*"},
		{"`a`", "~a~"},
		{"_a_b_", "/a b/"},
		{"_a__b_", "/a_b/"},
		{"_a___b_", "/a_ b/"},
		{"*a**b*?", "*a*b*?"},
		{"_a_<>_b_.", "/a <> b/."},
		{"(_a_)", "(/a/)"},
		{"((_a_), _b_, _c_).", "((/a/), /b/, /c/)."},
		{"(_a)", "(_a)"},
		{"(_a)", "(_a)"},
		{"_Why_use_scoped__ptr_? Use plain ***ptr* instead.",
			"/Why use scoped_ptr/? Use plain **ptr* instead."},
		{"_hey_ [[http://golang.org][*Gophers*]] *around*",
			`/hey/ [[http://golang.org][*Gophers*]] *around*`},
		{"_hey_ [[http://golang.org][so _many_ *Gophers*]] *around*",
			`/hey/ [[http://golang.org][so _many_ *Gophers*]] *around*`},
		{"Visit [[http://golang.org]] now",
			`Visit [[http://golang.org]] now`},
		{"my talk ([[http://talks.golang.org/][slides here]])",
			`my talk ([[http://talks.golang.org/][slides here]])`},
		{"Markup—_especially_italic_text_—can easily be overused.",
			`Markup—/especially italic text/—can easily be overused.`},
		{"`go`get`'s codebase", // ascii U+0027 ' before s
			`~go get~'s codebase`},
		{"`go`get`’s codebase", // unicode right single quote U+2019 ’ before s
			`~go get~’s codebase`},
		{"a_variable_name", `a_variable_name`},
		// newline
		{"Why use a scoped pointer?\nUse plain ptr instead.",
			"Why use a scoped pointer?\nUse plain ptr instead."},
	}
	for _, test := range tests {
		out := obFont(test.in)
		if out != test.out {
			t.Errorf("font(%q):\ngot\t%q\nwant\t%q", test.in, out, test.out)
		}
	}
}


func TestFindObRoot(t *testing.T) {
	root, err := findObRoot()
	if err != nil {
		t.Errorf("Failed: %v", err)
	}
	if filepath.Base(root) != "obtour" {
		t.Errorf("Failed: %v", root)
	}
}

func TestFindTourRoot(t *testing.T) {
	root, err := findTourRoot()
	if err != nil {
		t.Errorf("Failed: %v", err)
	}
	if ! strings.HasPrefix(filepath.Base(root), "website") {
		t.Errorf("Failed: %v", root)
	}
}

// TODO figure out how to ignore tests unless explicitly requested. The next
// two tests were originally written to ferret out a bug involving CRLF line
// endings. However, in the end, the bug was not even Go-related. Still, these
// might come in handy later.
//
// Note that present.Parse parses the marked up present doc and NOT the
// template.

const someMarkup = `
SomeTitle!
SomeDescription.

* SomeHeader
This is the first line of the first
paragraph. This is the second line.

And this is the first line of the second paragraph.
`

func TestUpstreamParse(t *testing.T) {
	dirPath, err := makeCurrentTempDir("upstreamParse")
	path := filepath.Join(dirPath, "some.article")
	if err != nil {
		t.Errorf("Failed: %v", err)
	}
	err = ioutil.WriteFile(path, []byte(strings.TrimLeft(someMarkup, "\r\n")), 0644)
	if err != nil {
		t.Errorf("Failed: %v", err)
	}
	reader := strings.NewReader(someMarkup)
	doc, err := present.Parse(reader, path, 0)
	if err != nil {
		t.Errorf("Failed: %v", err)
	}
	var sec present.Section = doc.Sections[0]
	if name := sec.Elem[0].TemplateName(); name != "text" {
		t.Errorf("Failed: %v", name)
	}
	first := sec.Elem[0].(present.Text).Lines[0]
	if first != "This is the first line of the first" {
		t.Errorf("Failed: %v", first)
	}
	second := sec.Elem[0].(present.Text).Lines[1]
	if second != "paragraph. This is the second line." {
		t.Errorf("Failed: %v", second)
	}
}


// Newlines are already recorded as content for text nodes
func TestStdlibTemplateParseFiles(t *testing.T) {
	tmpl := template.Must(createTmpl("./template/multi.tmpl"))
	s := reflect.ValueOf(tmpl).Elem()
	tmplMap := s.FieldByName("tmpl")
	sec := tmplMap.MapIndex(reflect.ValueOf("section"))
	tree := sec.Elem().FieldByName("Tree")
	root := tree.Elem().FieldByName("Root")
	nodes := root.Elem().FieldByName("Nodes")
	node := nodes.Index(0)
	text := node.Elem().Elem().FieldByName("Text")
	if string(text.Bytes()) != "\n" {
		t.Errorf("Failed: %v", text)
	}
}
