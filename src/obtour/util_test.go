package obtour

import "testing"

func TestReduce(t *testing.T) {
	sample := []string{"1", "2"}
	res := reduce(
		func (s string, t string) string { return s + t }, sample,
	)
	if res != "12" || sample[0] != "1" {
		t.Errorf("Failed")
	}
	res = reduce(
		func (s string, t string) string { return s + t }, []string{"1"},
	)
	if res != "1" {
		t.Errorf("Failed")
	}
}

func TestMapUniq(t *testing.T) {
	res, err := mapUniq(
		func (s string) string { return "a" }, []string{"1", "2"},
	)
	if res != "a" || err != nil {
		t.Errorf("Failed")
	}
	res, err = mapUniq(
		func (s string) string { return s }, []string{"1", "2"},
	)
	if res != "" || err == nil {
		t.Errorf("Failed")
	}
}
