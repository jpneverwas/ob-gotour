// +build js,wasm
package scrape

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"syscall/js"
)

var scrapePat *regexp.Regexp = regexp.MustCompile(
	`(?m)(?:value[(].*tableOfContents[^[]+)(\[[^)]+)`,
)

func scrapeValuesJs(staticPath string) ([]byte, error) {
	f, err := os.Open(filepath.Join(staticPath, "js", "values.js"))
	if err != nil {
		return nil, err
	}
	defer f.Close()
	reader := bufio.NewReader(f)
	bounds := scrapePat.FindReaderSubmatchIndex(reader)
	if bounds == nil {
		return nil, fmt.Errorf("Problem finding pat")
	}
	if len(bounds) != 4 {
		return nil, fmt.Errorf("Problem finding subs: %v", bounds)
	}
	start, end := bounds[2], bounds[3] // these come in pairs, we want 2nd grp
	f.Seek(int64(start), 0)
	buf := make([]byte, end - start)
	f.Read(buf)
	return buf, nil
}


func stringifyValuesJs(src string) string {
	global := js.Global()
	obj := global.Call("eval", src)
	out := global.Get("JSON").Call("stringify", obj)
	return out.String()
}
