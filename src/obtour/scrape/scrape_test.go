package scrape

// You must be able to summon node (a Node Js interpreter) and the executable
// go_js_wasm_exec. It might be helpful to link the latter from
// /usr/lib/go/misc/wasm to .local/bin (or wherever). The env var TOUR_ROOT
// can be used to override the automatic search for golang.org/x/tour. GOOS
// and GOARCH should be exported as follows:
//
// >$ TOUR_ROOT=~/go/src/golang.org/x/tour GOOS=js GOARCH=wasm go test
//
// See syscall/js/js_test.go
//
// More recently, this is crocked. Something to do with excess env vars in the
// virtual memory for the process. Also, static assets have moved to
// golang.org/x/website.
//
// >$ env -i TOUR_ROOT=~/go/src/golang.org/x/website \
// HOME=$HOME PATH=$PATH GOOS=js GOARCH=wasm go test

import (
	"os"
	"path/filepath"
	"testing"
	"fmt"

	"obtour/scrape/toc"
)

var tourRoot, staticPath string

func TestScrapeValuesJs(t *testing.T) {
	b, err := scrapeValuesJs(staticPath)
	if err != nil {
		t.Error(err)
	}
	if b[0] != '[' || b[len(b) -1] != ']' {
		t.Errorf("Failed: %v", string(b))
	}
}

func TestEvalValuesJs(t *testing.T) {
	got := stringifyValuesJs(`([{a:1},{b:2}])`)
	want := `[{"a":1},{"b":2}]`
	if got != want {
		t.Errorf("got %#v, want %#v", got, want)
	}
}

func TestScrapeValueJs(t *testing.T) {
	b, err := scrapeValuesJs(staticPath)
	if err != nil {
		t.Error(err)
	}
	got := stringifyValuesJs(string(b))
	entries, err := toc.UnpackStringified(got)
	if err != nil {
		t.Error(err)
	}
	if len(entries) < 4 {
		t.Errorf("Missing entries: %#v", entries)
	}
	if entries[0].Id != "mechanics" {
		t.Errorf("Bad entry: %#v", entries)
	}
}


func TestUpdateLocalCopy(t *testing.T) {
	thisDir, err := filepath.Abs(".")
	if err != nil {
		t.Error(err)
	}
	if filepath.Base(thisDir) != "scrape" {
		t.Errorf("Lost: %v", thisDir)
	}
	dest := filepath.Join(thisDir, "values.json")
	// save
	b, err := scrapeValuesJs(staticPath)
	if err != nil {
		t.Error(err)
	}
	encoded := stringifyValuesJs(string(b))
	toc.UpdateLocalCopy(dest, encoded)
}


func init() {
	maybeRoot := os.Getenv("TOUR_ROOT")
	if maybeRoot == "" {
		fmt.Println("TOUR_ROOT empty or not exported")
		os.Exit(1)
	}
	maybeStaticPath := filepath.Join(maybeRoot, "_content/tour/static")
	maybeValues := filepath.Join(maybeStaticPath, "/js/values.js")
	if _, err := os.Stat(maybeValues); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	tourRoot = maybeRoot
	staticPath = maybeStaticPath
}
