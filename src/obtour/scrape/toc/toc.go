package toc

import (
	"encoding/json"
	"os"
)


type TocEntry struct {
	Id string
	Title string
	Description string
	Lessons []string
}


func UnpackStringified(encoded string) ([]TocEntry, error) {
	var entries []TocEntry
	err := json.Unmarshal([]byte(encoded), &entries)
	if err != nil {
		return nil, err
	}
	return entries, nil
}


// Save latest object locally
func UpdateLocalCopy(path string, encoded string) error {
	f, err := os.Create(path)
	if err != nil {
		return err
	}
	defer f.Close()
	f.WriteString(encoded)
	return nil
}

