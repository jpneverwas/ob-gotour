package obtour

import "fmt"

// reduce is plain old reduce for a slice of strings
func reduce(f func(s string, t string) string, elts []string) string {
	var s string
	if len(elts) > 1 {
		s = elts[0]
		elts = elts[1:]
	}
	for _, t := range elts {
		s = f(s, t)
	}
	return s
}

// mapUniq returns whether all strings are identical after applying f
func mapUniq(f func (s string) string, strSet []string) (string, error) {
	var badA, badB string
	sentinel := "mapUniq____SENTINEL"
	base := f(strSet[0])
	g := func(s, t string) string {
		if s == sentinel {
			return sentinel
		}
		if base != f(t) {
			badA, badB = s, t
			return sentinel
		}
		return s
	}
	res := reduce(g, strSet)
	if res == sentinel {
		return "", fmt.Errorf("after applying f, %v != %v", badA, badB)
	}
	return base, nil
}
