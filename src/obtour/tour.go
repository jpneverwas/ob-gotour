package obtour

// This file is licensed under the GO_LICENSE found in this directory. The
// content below is copied verbatim from golang.org/x/tour tour.go and
// local.go

// The real /x/tour package is a "command" (package main), so it can't be
// imported. Even if that weren't the case, almost all names are unexported.

import (
	"os"
	"path/filepath"
	"text/template"

	"golang.org/x/tools/present"
)

var (
	basePkg = "obtour"
	funcs   = template.FuncMap{}
	lessons = make(map[string][]byte)
)

// File defines the JSON form of a code file in a page.
type File struct {
	Name    string
	Content string
	Hash    string
}

// Page defines the JSON form of a tour lesson page.
type Page struct {
	Title   string
	Content string
	Files   []File
}

// Lesson defines the JSON form of a tour lesson.
type Lesson struct {
	Title       string
	Description string
	Pages       []Page
}

// findPlayCode returns a slide with all the Code elements in the given
// Elem with Play set to true.
func findPlayCode(e present.Elem) []*present.Code {
	var r []*present.Code
	switch v := e.(type) {
	case present.Code:
		if v.Play {
			r = append(r, &v)
		}
	case present.Section:
		for _, s := range v.Elem {
			r = append(r, findPlayCode(s)...)
		}
	}
	return r
}

// isRoot reports whether path is the root directory of the tour tree.
// To be the root, it must have content and template subdirectories.
func isRoot(path string) bool {
	_, err := os.Stat(filepath.Join(path, "_content/tour", "welcome.article"))
	if err == nil {
		_, err = os.Stat(
			filepath.Join(path, "_content/tour/template", "index.tmpl"),
		)
	}
	return err == nil
}
