package obtour

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"
	"regexp"
	"strings"
	"testing"
	"text/template"

	"obtour/scrape/toc"
)

var tempDirRoot string = filepath.Join(os.TempDir(), "obtour-test")
var tourRoot string
var welPath string

func TestBadPreRegexp(t *testing.T) {
	var tests = []struct {
		in  string
		out []string
	}{
		{"", nil},
		{" ", nil},
		{"abc", nil},
		{"  def", nil},
		{"*abc", []string{"*abc", "", "*abc"}},
		{",*abc", []string{",*abc", "", ",*abc"}},
		{"  *abc", []string{"  *abc", "  ", "*abc"}},
		{"  ,*abc", []string{"  ,*abc", "  ", ",*abc"}},
		{"  #+abc", []string{"  #+abc", "  ", "#+abc"}},
		{"  ,#+abc", []string{"  ,#+abc", "  ", ",#+abc"}},
	}
	for _, test := range tests {
		out := badPrePat.FindStringSubmatch(test.in)
		if !reflect.DeepEqual(out, test.out) {
			t.Errorf("f(%q):\ngot\t%q\nwant\t%q", test.in, out, test.out)
		}
	}
}

var CleanPre = cleanPre

func ExampleCleanPre() {
	const src = `
*p = 21
for {
	/*
	these
	,* prefixes
	#+ aren't
	,#+ allowed
    right?
	*/
}
`
	fmt.Println(cleanPre(src))
	// Output:
	// 	,*p = 21
	// 	for {
	// 		/*
	// 		these
	// 		,,* prefixes
	// 		,#+ aren't
	// 		,,#+ allowed
	// 		right?
	// 		,*/
	// 	}
}

func TestCreateTmpl(t *testing.T) {
	tmpl, err := createTmpl("./template/multi.tmpl")
	if err != nil {
		t.Errorf("Failed: %v", err)
	}
	if tmpl.Name() != "root" {
		t.Errorf("Failed: %v", tmpl.Name())
	}
	rawTmpl, err := ioutil.ReadFile("./template/multi.tmpl")
	if err != nil {
		t.Errorf("Failed: %v", err)
	}
	if bytes.Contains(rawTmpl, []uint8{'\r', '\n'}) {
		t.Errorf("Failed: %q", rawTmpl[:30])
	}
}

func TestGetArticlePaths(t *testing.T) {
	matches := getArticlePaths(filepath.Join(tourRoot, "_content/tour"))
	expect := []string{
		"basics.article",
		"concurrency.article",
		"flowcontrol.article",
		"generics.article",
		"methods.article",
		"moretypes.article",
		"welcome.article",
	}
	for i, match := range matches {
		base := filepath.Base(match)
		if expect[i] != base {
			t.Fatalf("want: %#v, got: %#v", expect[i], match)
		}
	}
}

func TestLesson(t *testing.T) {
	if len(lessons) != 0 {
		t.Errorf("Failed: %v", lessons)
	}

	const snip = `
{{define "section"}}
TITLE: {{.Title}}
{{end}}
`
	tmpl, err := template.New("test").Funcs(funcs).Parse(snip)
	if err != nil {
		t.Errorf("Failed: %v", err)
	}
	content, err := parseObLesson(tmpl, welPath)
	if err != nil {
		os.Stderr.Write(content)
		t.Errorf("Failed: %v", err)
	}

	lesson, err := decodeObLesson(content)
	if err != nil {
		t.Errorf("Failed: %v", err)
	}
	if lesson.Pages[0].Content != "TITLE: Hello, 世界" {
		t.Errorf("Failed: %q", lesson.Pages[0].Content)
	}
}

func TestJoinPageContent(t *testing.T) {
	p1 := Page{Content: "a"}
	p2 := Page{Content: "b"}
	pages := []Page{p1, p2}
	joined, err := joinPageContent(pages)
	if err != nil {
		t.Errorf("Failed: %v", err)
	}
	if string(joined) != "\na\n\nb\n" {
		t.Errorf("Failed: %s", joined)
	}
}

func TestInitObLessons(t *testing.T) {
	lessers := make(map[string][]byte)
	tmpl := template.Must(createTmpl("./template/multi.tmpl"))

	// Ensure welcome article doesn't have CRLF endings (nt)
	article, err := ioutil.ReadFile(welPath)
	if err != nil {
		t.Errorf("Failed: %v", err)
	}
	if bytes.Contains(article, []uint8{'\r', '\n'}) {
		t.Errorf("Failed: %q", article[:30])
	}
	// Collect articles
	articles := []string{welPath}
	if err := initObLessons(tmpl, articles, lessers); err != nil {
		t.Errorf("Failed to init lessons: %v", err)
	}
	if len(lessers) != 1 {
		t.Errorf("Failed: %v", lessers)
	}
	encoded, ok := lessers["welcome"]
	if !ok {
		t.Errorf("Welcome not in lessers: %v", lessers)
	}
	lesson, err := decodeObLesson(encoded)
	if err != nil {
		t.Errorf("Failed: %v", err)
	}
	content := lesson.Pages[0].Content
	// Ends are trimmed
	if !strings.HasPrefix(content, "** Hello") {
		t.Errorf("Failed: %q", content[:10])
	}
	if !strings.HasSuffix(content, "END_SRC") {
		t.Errorf("Failed: %q", content[len(content) - 10:])
	}
	if m, err := regexp.MatchString("\r\n", content); m || err != nil {
		t.Errorf("Failed: %q", content[:30])
	}

	// Global lessons untouched
	if len(lessons) != 0 {
		t.Errorf("Failed: %v", lessons)
	}
}

func TestExportObLessons(t *testing.T) {
	lessers := make(map[string][]byte)
	tmpl := template.Must(createTmpl("./template/multi.tmpl"))

	articles := []string{welPath}
	if err := initObLessons(tmpl, articles, lessers); err != nil {
		t.Errorf("Failed to init lessons: %v", err)
	}
	if len(lessers) != 1 {
		t.Errorf("Failed: %v", lessers)
	}
	scratch, err := makeCurrentTempDir("ExportObLessons")
	if err != nil {
		t.Fatal(err)
	}
	tocEnts := []toc.TocEntry{
		{
			Id:          "mechanics",
			Title:       "Using the tour",
			Description: "Blah...",
			Lessons:     []string{"welcome"},
		},
	}
	fmt.Printf("Exporting to: %s\n", scratch)
	if err := exportObLessons(scratch, tocEnts, lessers); err != nil {
		t.Fatalf("Problem exporting: %v", err)
	}
}

func TestObMain(t *testing.T) {
	if buildDir != "" {
		t.Fatalf("buildDir already set: %v", buildDir)
	}
	scratch, err := makeCurrentTempDir("ObMain")
	if err != nil {
		t.Fatal(err)
	}
	buildDir = scratch
	obMain()
}

func TestLoadCachedJsonValues(t *testing.T) {
	path := "./scrape/values.json"
	entries, err := loadCachedJSONValues(path)
	if err != nil {
		t.Fatal(err)
	}
	if entries[0].Id != "mechanics" {
		t.Errorf("Bad entry: %#v", entries)
	}
}

// Unused
func TestUnHtmlize(t *testing.T) {
	const orig = `<p>Welcome to a tour of the <a href="https://golang.org">Go programming language</a>. The tour covers the most important features of the language, mainly:</p>`
	const want = `Welcome to a tour of the [[https://golang.org][Go programming language]]. The tour covers the most important features of the language, mainly:`
	got := unHtmlize(want)
	if got != want {
		t.Errorf("Got: %#v, Want: %#v", got, want)
	}
}

func makeCurrentTempDir(name string) (string, error) {
	scratch, err := ioutil.TempDir(tempDirRoot, name)
	if err != nil {
		return "", err
	}
	linkTarget := filepath.Join(tempDirRoot, name)
	os.Remove(linkTarget)
	os.Symlink(filepath.Base(scratch), linkTarget)
	return scratch, nil
}

func copyImages(imgDir string) error {
	p := filepath.Join(tourRoot, "_content/tour/*/img/*")
	imagePaths, err := filepath.Glob(p)
	if err != nil {
		return err
	}
	if len(imagePaths) == 0 {
		return fmt.Errorf("Empty imagePaths: %s", p)
	}
	for _, srcPath := range imagePaths {
		src, err := os.Open(srcPath)
		if err != nil {
			return err
		}
		dstPath := filepath.Join(imgDir, filepath.Base(srcPath))
		dst, err := os.Create(dstPath)
		if err != nil {
			return err
		}
		_, err = io.Copy(dst, src)
		if err != nil {
			return err
		}
		src.Close()
		dst.Close()
	}
	return nil
}

func init() {
	errf := func(f string, a ...interface{}) {
		if _, err := fmt.Fprintf(os.Stderr, f, a...); err != nil {
			fmt.Println(err, fmt.Sprintf(f, a...))
		}
		os.Exit(1)
	}
	if goTestTmpDir := os.Getenv(
		"GO_TEST_TMPDIR",
	); goTestTmpDir != "" && goTestTmpDir != tempDirRoot {
		errf("%s differs from %s", goTestTmpDir, tempDirRoot)
	}
	err := os.Mkdir(tempDirRoot, 0700)
	if err != nil && !os.IsExist(err) {
		errf(err.Error())
	}
	imgDir := filepath.Join(tempDirRoot, "images")
	err = os.Mkdir(imgDir, 0700)
	if err != nil && !os.IsExist(err) {
		errf(err.Error())
	}
	tourRoot, err = findTourRoot()
	if err != nil {
		errf(err.Error())
	}
	err = copyImages(imgDir)
	if err != nil {
		errf(err.Error())
	}
	welPath = filepath.Join(tourRoot, "_content/tour", "welcome.article")
	if _, err := os.Stat(welPath); err != nil {
		errf(err.Error())
	}
}
