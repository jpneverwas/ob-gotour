package obtour

// This file is licensed under the GO_LICENSE found in this directory. It
// contains tweaked versions of mostly unexported names from the following
// packages:
//
// golang.org/x/tour
// golang.org/x/tools/go/packages
// golang.org/x/tools/present
//
// Unfortunately, some function names have been modified, which is confusing.

import (
	"bytes"
	"crypto/sha1"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"go/build"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
	"text/template"
	"unicode"
	"unicode/utf8"

	"golang.org/x/tools/go/packages"
	"golang.org/x/tools/present"
)

/*
 * golang.org/x/tools/present style.go
 */

// See namesake in present/style.go; this is identical except for the link part
func split(s string) []string {
	var (
		words = make([]string, 0, 10)
		start = 0
	)

	// appendWord appends the string s[start:end] to the words slice.
	// If the word contains the beginning of a link, the non-link portion
	// of the word and the entire link are appended as separate words,
	// and the start index is advanced to the end of the link.
	appendWord := func(end int) {
		if j := strings.Index(s[start:end], "[["); j > -1 {
			l := strings.Index(s[start+j:], "]]") + 2
			if l == 1 {
				return
			}
			// Append portion before link, if any.
			if j > 0 {
				words = append(words, s[start:start+j])
			}
			// Append link itself.
			words = append(words, s[start+j:start+j+l])
			// Advance start index to end of link.
			start = start + j + l
			return
		}
		// No link; just add the word.
		words = append(words, s[start:end])
		start = end
	}

	wasSpace := false
	for i, r := range s {
		isSpace := unicode.IsSpace(r)
		if i > start && isSpace != wasSpace {
			appendWord(i)
		}
		wasSpace = isSpace
	}
	for start < len(s) {
		appendWord(len(s))
	}
	return words
}

// ctinner is a helper for obFont.
//
// The main takeaway is that these articles have their own markup rules, and
// everything between two style delims must obey those rules. For example, org
// mode is smart enough to preserve and respect intervening delims, so
// //usr/share/foo/ becomes an italicized <i>/usr/share/foo</i>. Here,
// __usr_share_foo_ becomes <i> usr share foo</i>. With these articles, "inner
// chars" are replaced with a space unless doubled (escaping mechanism).
func ctinner(b *bytes.Buffer, head string, char byte) {
	var wid int
	for i := 1; i < len(head)-1; i += wid {
		var r rune
		r, wid = utf8.DecodeRuneInString(head[i:])
		if r != rune(char) {
			// Ordinary character.
			b.WriteRune(r)
			continue
		}
		if head[i+1] != char {
			// Inner char becomes space.
			b.WriteRune(' ')
			continue
		}
		// Doubled char becomes real char.
		// Not worth worrying about "_x__".
		b.WriteByte(char)
		wid++ // Consumed two chars, both ASCII.
	}
}

// obFont is an ob-specific version of font() from the present package styles
// file. Here is the original function's doc comment: font returns s with font
// indicators turned into HTML font tags.
func obFont(s string) string {
	if !strings.ContainsAny(s, "`_*") {
		return s
	}
	words := split(s)
	var b bytes.Buffer
	for w, word := range words {
		if len(word) < 2 {
			continue
		}
		// Initial punctuation is OK but must be peeled off.
		first := strings.IndexAny(word, "_*`")
		if first == -1 {
			continue
		}
		// Opening marker must be at the beginning of the token or else preceded by punctuation.
		if first != 0 {
			r, _ := utf8.DecodeLastRuneInString(word[:first])
			if !unicode.IsPunct(r) {
				continue
			}
		}
		open, word := word[:first], word[first:]
		char := word[0] // ASCII is OK.
		close := ""
		switch char {
		default:
			log.Fatalf("Bad programming:%v in %v", word, words)
		case '_':
			open += "/"
			close = "/"
		case '*':
			open += "*"
			close = "*"
		case '`':
			open += "~"
			close = "~"
		}
		// Closing marker must be at the end of the token or else followed by punctuation.
		last := strings.LastIndex(word, word[:1])
		if last == 0 {
			continue
		}
		if last+1 != len(word) {
			r, _ := utf8.DecodeRuneInString(word[last+1:])
			if !unicode.IsPunct(r) {
				continue
			}
		}
		head, tail := word[:last+1], word[last+1:]
		b.Reset()
		b.WriteString(open)
		ctinner(&b, head, char)
		b.WriteString(close) // Write closing tag.
		b.WriteString(tail)  // Restore trailing punctuation.
		words[w] = b.String()
	}
	return strings.Join(words, "")
}

/*
 * golang.org/x/tools/present parse.go
 */

// execTemplate is a helper to execute a template and return the output
func execTemplate(t *template.Template, name string, data interface{}) (string, error) {
	b := new(bytes.Buffer)
	err := t.ExecuteTemplate(b, name, data)
	if err != nil {
		return "", err
	}
	return strings.TrimSpace(b.String()), nil
}

// renderElem implements the elem template function, used to render
// sub-templates.
func renderElem(t *template.Template, e present.Elem) (string, error) {
	var data interface{} = e
	if s, ok := e.(present.Section); ok {
		data = struct {
			present.Section
			Template *template.Template
		}{s, t}
	}
	return execTemplate(t, e.TemplateName(), data)
}

/*
 * golang.org/x/tour tour.go
 */

// renderOb is an ob version of render() from the go tour package
func renderOb(s *present.Section, w io.Writer, t *template.Template) error {
	data := struct {
		*present.Section
		Template    *template.Template
		PlayEnabled bool
	}{s, t, present.PlayEnabled}
	return t.ExecuteTemplate(w, "section", data)
}

// createLesson creates a lesson from a populated Document
func createLesson(tmpl *template.Template, doc *present.Doc) (Lesson, error) {
	lesson := Lesson{
		Title:       doc.Title,
		Description: doc.Subtitle,
		Pages:       make([]Page, len(doc.Sections)),
	}

	for i, sec := range doc.Sections {
		p := &lesson.Pages[i]
		w := new(bytes.Buffer)
		if err := renderOb(&sec, w, tmpl); err != nil {
			return lesson, fmt.Errorf("render section: %v", err)
		}
		p.Title = sec.Title
		p.Content = strings.Trim(w.String(), "\r\n")
		codes := findPlayCode(sec)
		p.Files = make([]File, len(codes))
		for i, c := range codes {
			f := &p.Files[i]
			f.Name = c.FileName
			f.Content = string(c.Raw)
			hash := sha1.Sum(c.Raw)
			f.Hash = base64.StdEncoding.EncodeToString(hash[:])
		}
	}
	return lesson, nil
}

// parseObLesson is an ob version of parseLesson() from go tour package
func parseObLesson(tmpl *template.Template, path string) ([]byte, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	doc, err := present.Parse(f, path, 0)
	if err != nil {
		return nil, err
	}
	lesson, err := createLesson(tmpl, doc)
	if err != nil {
		return nil, err
	}
	w := new(bytes.Buffer)
	if err := json.NewEncoder(w).Encode(lesson); err != nil {
		return nil, fmt.Errorf("encode lesson: %v", err)
	}
	return w.Bytes(), nil
}


/*
 * golang.org/x/tour local.go
 */

// isTourRoot wraps isRoot from golang.org/x/tour local.go
func isTourRoot(s string) bool { return isRoot(s) }

// findTourRoot returns root of upstream pkg golang.org/x/tour
func findTourRoot() (string, error) {
	pkgs, err := packages.Load(nil, "golang.org/x/website")
	if err != nil {
		return "", err
	}
	if len(pkgs) != 1 {
		return "", fmt.Errorf("Couldn't find golang.org/x/website\n")
	}
	files := pkgs[0].GoFiles
	if len(files) == 0 {
		return "", fmt.Errorf("Could not locate tour source files\n")
	}
	root, err := mapUniq(filepath.Dir, files)
	if err != nil {
		return "", err
	}
	if !isTourRoot(root) {
		return "", fmt.Errorf("Invalid path %v given as tour root\n", root)
	}
	return root, nil
}

// isObRoot is ob version of golang.org/x/tour unexported isRoot; it reports
// whether path is the root directory of the obtour tree.
func isObRoot(path string) bool {
	_, err := os.Stat(filepath.Join(path, "scrape", "values.json"))
	if err == nil {
		_, err = os.Stat(filepath.Join(path, "template", "multi.tmpl"))
	}
	return err == nil
}

// findRoot was copied verbatim from golang.org/x/tour local.go
func findObRoot() (string, error) {
	ctx := build.Default
	p, err := ctx.Import(basePkg, "", build.FindOnly)
	if err == nil && isObRoot(p.Dir) {
		return p.Dir, nil
	}
	return "", fmt.Errorf("could not find go-tour content; check $GOROOT and $GOPATH")
}
