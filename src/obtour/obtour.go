package obtour

// Org-oriented text analogs for HTML template functions in tour.go

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"text/template"

	"obtour/scrape/toc"

	"golang.org/x/tools/present"
)

// Set this to if removing TOC parts
const globalLevelOffset = 1

var buildDir string

func genHeadingMarkup(nevel []int) string {
	n := present.Section{Number: nevel}.Level()
	buf := []byte{}
	for i := 0; i < n; i++ {
		buf = append(buf, '*')
	}
	return string(buf)
}

var badPrePat *regexp.Regexp = regexp.MustCompile(`^(\s*)(,?(?:[*]|#[+]).*)$`)
var tabPrePat *regexp.Regexp = regexp.MustCompile(`^([ ]{4})+(.*)$`)

// Escape leading reserved markup in source blocks * ,* #+ and ,#+
func cleanPre(s string) string {
	// Some of the .pre examples have leading spaces, but some of those aren't
	// runnable, meaning we can't use go/ast,parser,etc., as tour/fmt.go does,
	// to easily fix this.
	lines := strings.Split(strings.TrimSpace(s), "\n")
	for i, line := range lines {
		if m := badPrePat.FindStringSubmatch(line) ; m != nil {
			line = m[1] + "," + m[2]
		}
		if len(line) != 0 {
			line = "\t" + tabPrePat.ReplaceAllString(line, "\t$2")
		}
		lines[i] = line
	}
	return strings.Join(lines, "\n")
}

func cleanCode(raw []byte) string {
	return cleanPre(string(raw))
}

// cleanText replaces `literal` and emphaisis-related delims with native org
// markup; luckily, valid go tokens don't begin or end with tilde ~; this
// function was ported from the unexported font() in present/style.go
func cleanText(s string) string { return obFont(s) }

// pageNum derives a page number from a section.
func pageNum(s present.Section, offset int) int {
	offset += globalLevelOffset
	if len(s.Number) == 0 {
		return offset
	}
	return s.Number[0] + offset
}

func getArticlePaths(dirpath string) []string {
	matches, err := filepath.Glob(dirpath + "/*.article")
	if err != nil {
		log.Fatal(err)
	}
	return matches
}

func initObLessons(
	tmpl *template.Template,
	articlePaths []string,
	lessons map[string][]byte,
) error {
	for _, path := range articlePaths {
		serializedLesson, err := parseObLesson(tmpl, path)
		if err != nil {
			return fmt.Errorf("parsing %v: %v", path, err)
		}
		name := strings.TrimSuffix(filepath.Base(path), ".article")
		lessons[name] = serializedLesson
	}
	return nil
}

func createTmpl(templatePath string) (*template.Template, error) {
	return template.New("root").Funcs(funcs).ParseFiles(templatePath)
}

func decodeObLesson(encoded []byte) (Lesson, error) {
	lesson := Lesson{
		Pages: make([]Page, 1),
	}
	buf := bytes.NewBuffer(encoded)
	if err := json.NewDecoder(buf).Decode(&lesson); err != nil {
		return lesson, err
	}
	return lesson, nil
}

func loadCachedJSONValues(path string) ([]toc.TocEntry, error){
	content, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	return toc.UnpackStringified(string(content))
}

func joinPageContent(pages []Page) ([]byte, error) {
	const pagesFmt = "{{range .}}\n{{.Content}}\n{{end}}"
	t := template.Must(template.New("t").Parse(pagesFmt))
	buf := &bytes.Buffer{}
	err := t.Execute(buf, pages)
	return buf.Bytes(), err
}

// See explanation below
const tocPartHeadingTmplShort = "\n* {{upper .Id}}\n"
const tocPartHeadingTmpl = `
* {{.Title}}

{{html .Description}}
`
const lessonHeadingTmpl = `
** {{.Title}}

{{.Description}}
`

const gopherImagePath = "static/img/gopher.png"
// const treeImagePath = "content/img/tree.png"

const imageDir = ".ob-gotour-images"

const titleLine = `#+TITLE: A Tour of Go`

var unHtmlizeParPat *regexp.Regexp = regexp.MustCompile(`(</?p>)`)
var unHtmlizeLinkPat *regexp.Regexp = regexp.MustCompile(
	`<a\s+href="(?P<url>[^"]+)">(?P<label>[^<]+)</a>`,
)

func convertImagePath(s string) string {
	return fmt.Sprintf("[[file:%s/%s]]", imageDir, filepath.Base(s))
}

// Remove element tags from content descriptions
func unHtmlize(s string) string {
	out := unHtmlizeLinkPat.ReplaceAllString(s, "[[${url}][${label}]]")
	return unHtmlizeParPat.ReplaceAllString(out, "")
}

// FIXME we should just pass the writer around
func exportLesson(lesson Lesson) ([]byte, error){
	t := template.Must(template.New("t").Funcs(funcs).Parse(lessonHeadingTmpl))
	buf := &bytes.Buffer{}
	err := t.Execute(buf, lesson)
	if err != nil {
		return nil, err
	}
	rest, err := joinPageContent(lesson.Pages)
	if err != nil {
		return rest, err
	}
	buf.Write(rest)
	return buf.Bytes(), nil
}

func exportPart(
	mainFile *os.File,
	buildRoot string,
	tocEntry toc.TocEntry,
	lessons map[string][]byte,
) error {
	for _, name := range tocEntry.Lessons {
		encoded := lessons[name]
		// Output the original json for other purposes
		metaDest := fmt.Sprintf("%s.lesson.json", name)
		metaFile, err := os.Create(filepath.Join(buildRoot, metaDest))
		if err != nil {
			return err
		}
		metaFile.Write(encoded)
		metaFile.Close()
		lesson, err := decodeObLesson(encoded)
		if err != nil {
			return err
		}
		joined, err := exportLesson(lesson)
		if err != nil {
			return err
		}
		mainFile.Write(joined)
	}
	return nil
}

// exportObLessons exports the whole shebang. The TOC heading group everything
// into a broader categories, but their titles and descriptions are redundant.
func exportObLessons(
	buildRoot string,
	tocEntries []toc.TocEntry,
	lessons map[string][]byte,
) error {
	err := os.Mkdir(buildRoot, 0700)
	if err != nil && !os.IsExist(err) {
		return fmt.Errorf("Creating dir: %v", err)
	}
	// This will be the org file
	mainDest := filepath.Join(buildRoot, "content.out")
	mainFile, err := os.Create(mainDest)
	if err != nil {
		return err
	}
	defer mainFile.Close()
	frontMatter := fmt.Sprintf(
		"%s\n\n%s\n", titleLine, convertImagePath(gopherImagePath),
	)
	mainFile.WriteString(frontMatter)
	pt := template.Must(
		template.New("parts").Funcs(funcs).Parse(tocPartHeadingTmplShort),
	)
	for _, tocEntry := range tocEntries {
		err := pt.Execute(mainFile, tocEntry)
		if err != nil {
			return err
		}
		exportPart(mainFile, buildRoot, tocEntry, lessons)
	}
	return nil
}

func obMain() {
	root, err := findObRoot()
	if err != nil {
		log.Fatalf("Couldn't find package root: %v", err)
	}
	tourRoot, err := findTourRoot()
	if err != nil {
		log.Fatalf("Couldn't find tour root: %v", err)
	}

	present.PlayEnabled = true

	// Set up templates.
	tmplPath := filepath.Join(root, "template", "multi.tmpl")
	tmpl := template.Must(createTmpl(tmplPath))

	// Get list of .article files
	articlePaths := getArticlePaths(filepath.Join(tourRoot, "_content/tour"))

	// Init lessons.
	if err := initObLessons(tmpl, articlePaths, lessons); err != nil {
		log.Fatalf("Failed to init lessons: %v", err)
	}

	if buildDir == "" {
		buildDir = filepath.Join(root, "build")
	}
	valuesFile := filepath.Join(root, "scrape/values.json")
	tocEnts, err := loadCachedJSONValues(valuesFile)
	if err != nil {
		log.Fatalf("Problem reading values.json: %v", err)
	}
	if err := exportObLessons(buildDir, tocEnts, lessons); err != nil {
		log.Fatalf("Problem exporting: %v", err)
	}
}

func init() {
	funcs["heading"] = genHeadingMarkup
	funcs["style"] = cleanText
	funcs["pre"] = cleanPre
	funcs["code"] = cleanCode
	funcs["elem"] = renderElem
	funcs["html"] = unHtmlize
	funcs["upper"] = strings.ToUpper
	funcs["pagenum"] = pageNum
	funcs["image"] = convertImagePath
}
