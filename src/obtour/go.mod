module obtour

go 1.18

require golang.org/x/tools v0.1.5

require (
	github.com/yuin/goldmark v1.3.5 // indirect
	golang.org/x/mod v0.4.2 // indirect
	golang.org/x/sys v0.0.0-20211124211545-fe61309f8881 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	golang.org/x/website v0.0.0-20220317152322-7b9aa998daa0 // indirect
)
