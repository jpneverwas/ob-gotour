#!/bin/bash
set -e

: "${DEST_PREFIX:=/usr/local}"
[[ -d $DEST_PREFIX ]]
[[ $GO_VERSION ]]
force=$1
arname=go$GO_VERSION.linux-amd64.tar.gz
target=${DEST_PREFIX%/}/$arname

if [[ ! -e $target ]] || (( force )); then
    curl -L -o "$target" "https://golang.org/dl/$arname"
fi
if [[ ! -x ${DEST_PREFIX%/}/go/bin/go ]] || (( force )); then
    cd $DEST_PREFIX && tar -xzf "$arname"
fi
if [[ -d $DEST_PREFIX/bin && ! -x $DEST_PREFIX/bin/go ]]; then
    cd $DEST_PREFIX/bin
    ln -s ../go/bin/* .
fi
