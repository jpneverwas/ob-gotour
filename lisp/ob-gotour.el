;;; ob-gotour.el --- Run go tour in org-mode source blocks -*- lexical-binding: t -*-

;; Copyright (C) F. Jason Park

;; Author: F. Jason Park <jp@neverwas.me>
;; Created: 01 Nov 2019
;; Version: 0.1.4
;; Package-Requires: ((emacs "27") (websocket "1.12") (aio "1.0"))

;; This file is not part of GNU Emacs. It is released under Apache 2.0. See
;; http://www.apache.org/licenses/LICENSE-2.0 for details.

;;; Commentary:

;; If you just now installed this, pick some directory where you want the Org
;; file and associated images deposited, then run M-x `ob-gotour-init' to do
;; the damage.  This may take a while, depending on your Go setup.  On
;; success, you should be presented with an Org buffer containing the tour.
;;
;; Use `org-ctrl-c-ctrl-c' inside source blocks labeled "gotour" to run the
;; block and show the results below.  The tour server will start the first
;; time you execute this in any Emacs session AND will stick around for the
;; duration.  In other words, killing a tour.org buffer leaves the server
;; running.  To kill it before Emacs, use the command `ob-gotour-shutdown'.
;;
;; If you think you may want to revisit your tour.org file in the future, Org
;; must be made aware of the fictitious "gotour" language.  Add the autoloaded
;; helper `ob-gotour-on-org-mode' as an `org-mode-hook' or just run it before
;; visitng the file.  See: (info "(org)Languages") for details.
;;
;; Variants
;;
;; A few variants of tour.org are provided, each with different formatting.
;; If none is to your liking, you can always opt for tour-raw.org and tweak it
;; al gusto.  Running M-x `ob-gotour-fill-all' will hard-wrap all overlong
;; lines to your `fill-column'.  Similarly, if you have `go-mode' installed,
;; M-x `ob-gotour-indent-all-content' will adjust the content indentation on
;; all source blocks to match `org-edit-src-content-indentation'.  At the
;; moment, the default ships with blocks indented to a single tab, which is
;; nonstandard, maybe even offensive.
;;
;; Options
;;
;; If you'd like to use an alternate command for starting the server, for
;; example, `docker -p $myhost:$myport run gotour', assign it to
;; `ob-gotour-alt-server-command' (comming soon), and adjust
;; `ob-gotour-tcp-addr' accordingly.  By the way, if all you're up to is
;; shielding your precious $HOME dir from your Go-learning self (versus, say,
;; poking at malware), various isolation options may exist that don't require
;; admin privileges.  On Linux, these tend to be namespaces(7)-based, two
;; popular options being bubblewrap and rootless Podman.
;;
;; Note: this package used to build and run the tour each session from an
;; isolated directory.  This has been dropped in favor of using an installed
;; version cached by us.
;;
;; Note: this project is too much of a hack to bother with migrations.  When
;; versions change and something doesn't work, try deleting the cache
;; directory.
;;
;; TODO
;;
;; - Add helper command similar to `ob-gotour-fill-all' that visits every
;;   source block and adjusts indentation according to user preferences
;;   `org-edit-src-content-indentation'
;;
;; - Support non-hello "play modes" as block params corresponding to the
;;   examples in the Playground's dropdown menu.  This already handles
;;   base64-encoded images, so we'd need:
;;
;;   - a character-oriented mode that supports ANSI escapes; for more
;;     complicated stuff involving input, users should convert to
;;     jupyter blocks
;;
;;   - multi-file handling with -- foo -- delimiters
;;
;;   - test/example runner
;;
;; - Add block-conversion integration for ob-jupyter (emacs-jupyter)
;;
;; - Learn about inline source blocks and inline babel calls
;;
;; - Use aio everywhere (not just in setup)

;;; Code:

(defconst ob-gotour-version "v0.1.4")

(require 'json)
(require 'eieio-base)
(require 'subr-x)
(require 'ob)
(require 'websocket)
(require 'aio)

(defgroup ob-gotour nil
  "Go tour for org source blocks."
  :prefix "ob-gotour"
  :version "27"
  :group 'languages)

(defcustom ob-gotour-cache-dir nil
  "Directory to download source files into.
The leaf will be created if it doesn't exist."
  :group 'ob-gotour
  :type 'string)

(defcustom ob-gotour-go-exe "go"
  "The preferred go exe."
  :group 'ob-gotour
  :type 'string)

(defcustom ob-gotour-tcp-addr '("127.0.0.1" 13999)
  "The host:port for the server to listen on as a LIST."
  :group 'ob-gotour
  :type 'list)

(defcustom ob-gotour-ws-endpoint-fmt "ws://%s:%d/socket"
  "This is the URL used by upstream for so-called local mode.
It's defined in local.go and used in the template index.tmpl.  In the
concatenated browser script, it becomes window.socketAddr."
  :group 'ob-gotour
  :type 'string)

(defcustom ob-gotour-srcdir nil
  "Local fork of the go-tour to run in place of latest.
If defined, whatever's checked out (or simply lives) here will be run
instead of a cached binary."
  :group 'ob-gotour
  :type 'string)

(defcustom ob-gotour-default-variant 'filled
  "Choice of default format variant to open with `ob-gotour-init'."
  :group 'ob-gotour
  :type '(choice (const filled) (const raw)))

(defvar ob-gotour-tempdir nil
  "Temporary directory used by this package.
Removed up on Emacs exit.")

(defvar ob-gotour--http-origin-fmt "http://%s:%d"
  "Rendered value for the required Origin header.")

(cl-defstruct ob-gotour--request
  "Object containing request info sent the tour server.
MARKER should point to the head of the source block and not the results
area.  The server wants string IDs, so the sender should convert them.
PARAMS are the source block params.  These are safe to modify in
response handlers but may interfere with `org-babel-insert-result' if
messed with elsewhere.  BODY is an opaque string.  Finalizer is called
by the default `ob-gotour--handle-end' when non-null.  It is passed the
request object, and the return value is unused."
  (id 0 :type integer)
  (marker nil :type (or null marker))
  (params nil :type list)
  (body "" :type string)
  (finalizer nil :type (or null function)))

(defclass ob-gotour--transport (eieio-singleton)
  ((id
    :type integer
    :initform 0
    :documentation "Message I.D. sent and returned as string")
   (proc
    :type (or null process)
    :initform nil
    :documentation "Server subprocess")
   (ws
    :type (or null websocket)
    :initform nil
    :documentation "The `websocket' struct representing connection")
   (reqs
    :type hash-table
    :initform (make-hash-table :test 'equal)
    :documentation "ID to `ob-gotour--request'"))
  "Global object representing service session")

;; TODO get rid of the transport class or at least its "methods" below.
;;
;; The "uniqueness" guarantee was the only reason for using a class here, and
;; there aren't any behaviors making particular use of the instance.  So
;; having the "methods" below listed in the help buffer is misleading because
;; users may think they're supposed to override them when the opposite is
;; true: they're all very "final."  Actually, since the server address is
;; hard-coded (and we don't bother searching for an unused port), a redundant
;; instance wouldn't even be able to bind, so there's REALLY no reason for a
;; class.

(declare-function org-element-at-point "org-element" ())
(declare-function org-element-property "org-element" (property element))
(declare-function org-element-context "org-element" (&optional element))
(declare-function org-display-inline-images "org"
                  (&optional include-linked refresh beg end))
(declare-function org-in-src-block-p "org" (&optional inside))
(declare-function org-get-tags "org" (&optional pos local))
(declare-function tar-untar-buffer "tar-mode" ())

(defvar ob-gotour--load-file-name load-file-name)

(defun ob-gotour--remove-temp-dir ()
  "Remove `ob-gotour-tempdir' if present."
  (when (and ob-gotour-tempdir
             (string-prefix-p temporary-file-directory ob-gotour-tempdir)
             (file-exists-p ob-gotour-tempdir))
    (delete-directory ob-gotour-tempdir 'recursive)))

(defun ob-gotour--call-go-sentinel (proc event)
  (with-current-buffer (process-buffer (process-get proc 'stderr))
    (pcase event
      ((rx (| "finished" "exited"))
       (goto-char (point-max))
       (insert "\n" (format "[exited %d]: %S" (process-exit-status proc)
                            (process-command proc))
               "\n")
       (aio-resolve (process-get proc 'promise) (lambda () proc)))
      (_ (insert "\n" "[event] " event)))))

;; TODO when global reporter present, update with every write.
(defun ob-gotour--call-go-filter (proc string)
  (with-current-buffer (process-buffer proc) (insert string)))

(defun ob-gotour--call-go (&rest args)
  "Call go wth ARGS and return trimmed output."
  (cl-assert ob-gotour-tempdir)
  (cl-assert (file-exists-p ob-gotour-tempdir))
  (let* ((errbuf (get-buffer-create "*ob-gotour-setup-stderr*"))
         (outbuf (get-buffer-create "*ob-gotour-setup-stdout*"))
         (errproc (make-pipe-process
                   :name "ob-gotour-setup-stderr"
                   :filter #'ob-gotour--call-go-filter
                   :buffer errbuf))
         (outproc (make-process
                   :name "ob-gotour-setup"
                   :buffer outbuf
                   :command (cons ob-gotour-go-exe args)
                   :sentinel #'ob-gotour--call-go-sentinel
                   :filter #'ob-gotour--call-go-filter
                   :stderr errproc)))
    (process-put outproc 'stderr errproc)
    (process-put outproc 'promise (aio-promise))
    (with-current-buffer errbuf
      (insert "\n" (format "[starting]: %S" (process-command outproc))
              "\n"))
    (with-current-buffer outbuf (erase-buffer))
    (process-get outproc 'promise)))

;; Note: `org-with-point-at' runs an expression in the buffer associated with
;; the passed-in marker while widening and restoring point/restriction.

(defmacro ob-gotour--with-result-keyword (req &rest body)
  "Inject request ID into `org-babel-results-keyword' via text props.
Call `org-with-point-at' REQ's marker and run BODY.  Remember:
modifications to text properties won't survive a buffer kill."
  (declare (indent 1))
  `(let ((org-babel-results-keyword
          (propertize org-babel-results-keyword 'id
                      (if (aio-promise-p ,req)
                          ,req
                        (ob-gotour--request-id ,req)))))
     (org-with-point-at (if (aio-promise-p ,req)
                            org-babel-current-src-block-location
                          (ob-gotour--request-marker ,req))
       ,@body)))

(defvar ob-gotour--pinned-tour-version-suffix
  "v0.0.0-20220317152322-7b9aa998daa0")

(defvar ob-gotour--cache-dir nil)

(define-inline ob-gotour--cache-dir ()
  (inline-quote (or ob-gotour--cache-dir ob-gotour-cache-dir)))

(defun ob-gotour--ensure-cache-dir ()
  "Create `ob-gotour-cache-dir' if necessary and return its path.
The \"share\" subdirectory holds downloaded versions of rendered tour
Org files from the project releases endpoint.  A subdirectory whose name
matches `ob-gotour--pinned-tour-version-suffix' is also created."
  (unless (or ob-gotour-cache-dir ob-gotour--cache-dir)
    (setq ob-gotour--cache-dir
          ;; For now, don't use xdg-cache-home on Windows because of
          ;; possible permissions issues involving a default location
          (cond ((eq system-type 'windows-nt)
                 (expand-file-name ".cache/"
                                   (file-name-directory
                                    (locate-library "ob-gotour"))))
                ((and (require 'xdg nil t) (fboundp 'xdg-cache-home))
                 (expand-file-name "ob-gotour/" (xdg-cache-home)))
                ;; Likely never runs but may need for other OS
                (t (expand-file-name ".ob-gotour/" user-emacs-directory)))))
  (unless (file-exists-p (ob-gotour--cache-dir))
    (message "Creating: %s" (ob-gotour--cache-dir))
    (make-directory (ob-gotour--cache-dir) (eq system-type 'windows-nt)))
  (unless (directory-name-p (ob-gotour--cache-dir))
    (setq ob-gotour--cache-dir
          (file-name-as-directory (ob-gotour--cache-dir))))
  (let ((assets-dir (expand-file-name "share/" (ob-gotour--cache-dir))))
    (unless (file-exists-p assets-dir)
      (make-directory assets-dir)))
  (when-let* ((path (expand-file-name ob-gotour--pinned-tour-version-suffix
                                      (ob-gotour--cache-dir)))
              ((not (file-exists-p path))))
    (make-directory path))
  (ob-gotour--cache-dir))

(defvar ob-gotour--cache-dir-sentinel nil)

(defun ob-gotour--cache-dir-populated-p ()
  "Return nonnil if cache dir has been populated."
  (and (ob-gotour--cache-dir)
       (file-exists-p (ob-gotour--cache-dir))
       (or ob-gotour--cache-dir-sentinel
           (when-let* ((name (concat ob-gotour--pinned-tour-version-suffix
                                     "/bin/tour"))
                       (exe (expand-file-name name (ob-gotour--cache-dir)))
                       ((file-executable-p exe)))
             (setq ob-gotour--cache-dir-sentinel exe)))))

(defvar ob-gotour--skip-temp-dir-hook nil)

(defun ob-gotour--ensure-temp-dir ()
  "Maybe create temporary directory.
With NOHOOK, don't remove it when exiting Emacs."
  (unless (and ob-gotour-tempdir (file-exists-p ob-gotour-tempdir))
    (setq ob-gotour-tempdir (make-temp-file "ob-gotour." t))
    (unless ob-gotour--skip-temp-dir-hook
      (add-hook 'kill-emacs-hook #'ob-gotour--remove-temp-dir)))
  (setq ob-gotour-tempdir (file-name-as-directory ob-gotour-tempdir)))

(defvar ob-gotour--pinned-tour-version
  (concat "golang.org/x/website/tour@" ob-gotour--pinned-tour-version-suffix)
  "The qualified import name of the tour package we want.")

(aio-defun ob-gotour--populate-cache-dir ()
  "Check for and maybe fetch all tour modules."
  (message "Populating cache. This may take a minute.")
  (let ((default-directory ob-gotour-tempdir)
        (process-environment (append process-environment nil))
        (path (expand-file-name ob-gotour--pinned-tour-version-suffix
                                (ob-gotour--cache-dir))))
    (ob-gotour--ensure-cache-dir)
    (cl-assert (file-exists-p path))
    ;; Install in-tour dependencies via old-fashioned GOPATH
    (setenv "GOPATH" path)
    (setenv "GO111MODULE" "off")
    (let ((proc (aio-await
                 (ob-gotour--call-go "get" "-d" "golang.org/x/tour"))))
      (unless (with-current-buffer (process-buffer (process-get proc 'stderr))
                (save-excursion
                  (goto-char (point-min))
                  (search-forward "no Go files in" nil t)))
        (error "Error calling go with %S" (process-command proc))))
    ;;
    (setenv "GO111MODULE" "on")
    (let ((proc (aio-await
                 (ob-gotour--call-go "install" ob-gotour--pinned-tour-version))))
      (unless (zerop (process-exit-status proc))
        (error "Error calling go with %S" (process-command proc))))
    (unless (ob-gotour--cache-dir-populated-p)
      (error "Problem installing tour executable in: %S" path))))

(defun ob-gotour--get-joined-tcp-addr ()
  "Return string of HOST:PORT or maybe just HOST."
  (string-join (mapcar (lambda (o) (format "%s" o))
                       ob-gotour-tcp-addr)
               ":"))

(defun ob-gotour--check-process-up (buf tcp-addr)
  (let ((waited 0)
        (maxwait 1000))
    (with-current-buffer buf
      (while (not (save-excursion (goto-char (point-min))
                                  (search-forward tcp-addr nil t)))
        (sleep-for 0.01)
        (when (< maxwait (cl-incf waited))
          (princ (concat (buffer-string) "\n"))
          (error "Could not connect to %s" tcp-addr))))))

(cl-defmethod ob-gotour--start-server ((inst ob-gotour--transport))
  "Start tour server from cached binary and store in INST."
  (cl-assert (ob-gotour--cache-dir-populated-p))
  (let ((tcp-addr (ob-gotour--get-joined-tcp-addr))
        (process-environment (append process-environment nil))
        (proc (oref inst proc))
        (name (expand-file-name (concat ob-gotour--pinned-tour-version-suffix
                                        "/bin/tour")
                                (ob-gotour--cache-dir)))
        buf)
    (setenv "GOPATH" (expand-file-name ob-gotour--pinned-tour-version-suffix
                                       (ob-gotour--cache-dir)))
    (cl-assert (or (null proc) (not (process-live-p proc))))
    ;; Booleans must either be lone flags or expressed as l=r pairs
    ;; See: https://golang.org/pkg/flag/#hdr-Command_line_flag_syntax
    (setq buf (generate-new-buffer " *ob-gotour-server*")
          proc (oset inst
                     proc
                     (start-file-process "ob-gotour-server"
                                         buf name
                                         "-http" tcp-addr
                                         "-openbrowser=0")))
    (set-process-query-on-exit-flag proc nil)
    (ob-gotour--check-process-up buf tcp-addr)))

(cl-defmethod ob-gotour--start-server ((inst ob-gotour--transport)
                                       &context (ob-gotour-srcdir string))
  "Start tour server from local source and stash in INST.
This runs when OB-GOTOUR-SRCDIR is set."
  (cl-assert (ob-gotour--cache-dir-populated-p))
  (let ((default-directory ob-gotour-srcdir)
        (tcp-addr (ob-gotour--get-joined-tcp-addr))
        (process-environment (append process-environment nil))
        (proc (oref inst proc))
        buf)
    (setenv "GOTMPDIR" ob-gotour-tempdir)
    (cl-assert (or (null proc) (not (process-live-p proc))))
    ;; Booleans must either be lone flags or expressed as l=r pairs
    ;; See: https://golang.org/pkg/flag/#hdr-Command_line_flag_syntax
    (setq buf (generate-new-buffer " *ob-gotour-server*")
          proc (oset inst
                     proc
                     (start-file-process
                      "ob-gotour-server"
                      buf
                      ob-gotour-go-exe "run"
                      "-http" tcp-addr
                      "-openbrowser=0")))
    (set-process-query-on-exit-flag proc nil)
    (ob-gotour--check-process-up buf tcp-addr)))

(defun ob-gotour--get-inst ()
  "Return global transport instance or nil."
  (let ((value (oref-default 'ob-gotour--transport singleton)))
    (unless (eq value (symbol-value 'eieio-unbound))
      value)))

(defmacro ob-gotour--json-parse-string (instr)
  "Parse INSTR in a backward-compatible way."
  (if (fboundp 'json-parse-string)
      `(json-parse-string ,instr
                          :object-type 'plist
                          :null-object nil
                          :false-object nil)
    `(let ((json-array-type 'vector)
           (json-object-type 'plist)
           (json-false nil))
       (json-read-from-string ,instr))))

(defun ob-gotour--on-message-handler (_websock frame)
  "Call output filter for FRAME based on message Kind.
Where :Kind is a field in the response object with possible values of
stderr, stdout, exit.  See methods named `ob-gotour-handle-:Kind' (where
`:Kind' is as described)."
  (when-let* ((msg (ob-gotour--json-parse-string (websocket-frame-text frame)))
              (reqs (oref (ob-gotour--get-inst) reqs))
              (key (string-to-number (plist-get msg :Id)))
              (req (gethash key reqs))
              (body (plist-get msg :Body)))
    (pcase (plist-get msg :Kind)
      ("stdout" (ob-gotour--handle-stdout req body))
      ("stderr" (ob-gotour--handle-stderr req body))
      ("end" (ob-gotour--handle-end req body))
      (_ (error "Impossible")))))

(defun ob-gotour--on-close-handler (websock)
  "Print a message saying the connection has closed.
WEBSOCK is a websocket obj."
  (message "Closed: %s" (websocket-url websock)))

(defun ob-gotour--open-connection (on-open on-message on-close)
  "Open connection with provided callbacks.
Return promise that resolves to web socket.  See `websocket-open' for
ON-OPEN, ON-MESSAGE, and ON-CLOSE.  The Origin header is required by
this particular server."
  (let ((ws-ep (apply #'format ob-gotour-ws-endpoint-fmt ob-gotour-tcp-addr))
        (orign (apply #'format ob-gotour--http-origin-fmt ob-gotour-tcp-addr))
        (on-close-mod (lambda (websock)
                        (funcall on-close websock)
                        (oset (ob-gotour--get-inst) ws nil)))
        (promise (aio-promise)))
    (setq on-open (lambda (ws)
                    (aio-resolve promise (lambda () (funcall on-open ws) ws))))
    (oset (make-instance 'ob-gotour--transport)
          ws (websocket-open ws-ep
                             :on-open on-open
                             :on-message on-message
                             :on-close on-close-mod
                             :custom-header-alist `(("Origin" . ,orign))))
    promise))

(aio-defun ob-gotour--ensure-connected ()
  "Start server and ensure local websocket connection.
If websocket doesn't exist, create one and block till it's ready.
Return websocket object."
  (ob-gotour--ensure-temp-dir)
  (ob-gotour--ensure-cache-dir)
  (unless ob-gotour-srcdir
    (unless (ob-gotour--cache-dir-populated-p)
      (aio-await (ob-gotour--populate-cache-dir))))
  (let* ((inst (make-instance 'ob-gotour--transport))
         (ws (oref inst ws))) ; 10 secs
    (unless (process-live-p (oref inst proc))
      (ob-gotour--start-server inst)
      (cl-assert (process-live-p (oref inst proc))))
    (if (and ws (websocket-p ws) (websocket-openp ws))
        ws
      (aio-await
       (ob-gotour--open-connection
        (lambda (ws) (message "Opened: %s" (websocket-url ws)))
        #'ob-gotour--on-message-handler
        #'ob-gotour--on-close-handler)))))

(defun ob-gotour-shutdown ()
  "Close local websocket and shut down server."
  (interactive)
  (with-slots (id proc ws reqs) (ob-gotour--get-inst)
    (when ws
      (websocket-close ws))
    (when proc
      (delete-process proc))
    (clrhash reqs)
    (setq ws nil
          proc nil
          id 0)))

(defun org-babel-expand-body:gotour (&rest rest)
  "Defer to `org-babel-expand-body:generic', passing REST."
  (apply #'org-babel-expand-body:generic rest))

(defvar ob-gotour--image-dirname ".ob-gotour-images")

(defun ob-gotour--save-image (body)
  "Save base64-encoded BODY as PNG to subdir of `default-directory'."
  (unless (file-directory-p ob-gotour--image-dirname)
    (make-directory ob-gotour--image-dirname))
  (let ((target (expand-file-name (concat (sha1 body) ".png")
                                  ob-gotour--image-dirname)))
    (with-temp-file target
      (insert (base64-decode-string body)))
    target))

;; FIXME why use cl-generic here?  Should replace with normal functions.

;; Though :Kind is orthogonal, it seems overly complicated to dispatch on it.
;; Better to have three separate handlers, which is more in keeping with the
;; Go philosophy (for now).

(defun ob-gotour--handle-stdout (req body)
  "Handler for dealing with stdout result BODY for REQ."
  (if (string-prefix-p "IMAGE:" body)
      (let ((path (ob-gotour--save-image (substring body 6))))
        (setq body (format "[[file:%s]]" path))
        ;; XXX not sure what value of append should be. Best leave it on, no?
        (ob-gotour--modify-result req body 'append t)
        (when (window-system)
          (ob-gotour--with-result-keyword req
            (goto-char (org-babel-where-is-src-block-result 'insert))
            (org-display-inline-images))))
    (ob-gotour--modify-result req body 'append)))

;; TODO maybe highlight errors, as done in the browser client
(defun ob-gotour--handle-stderr (req body)
  "Handler for dealing with stderr result BODY for REQ."
  (ob-gotour--modify-result req body 'append))

(defun ob-gotour--handle-end (req _body)
  "Handler for dealing with end message for REQ.
BODY should be nil or the empty string."
  (when-let ((finalizer (ob-gotour--request-finalizer req)))
    (funcall finalizer req))
  (let* ((inst (ob-gotour--get-inst))
         (loc (ob-gotour--request-marker req)))
    (ob-gotour--with-result-keyword req (ob-gotour--remove-pending-tag))
    (set-marker loc nil) ; unnecessary
    (remhash (ob-gotour--request-id req) (oref inst reqs))))

(defvar ob-gotour--pending-text "# Pending..."
  "Shown in place of results block while awaiting first response.")

(defvar ob-gotour--pending-text-regexp
  (concat "^" (regexp-quote ob-gotour--pending-text) "$")
  "Regexp for finding `ob-gotour--pending-text'.")

(defun ob-gotour--get-pending-text (id)
  "Return placeholder text with ID text property."
  (propertize ob-gotour--pending-text
              'type 'ob-gotour--request-pending
              'id id))

(defun ob-gotour--goto-src-block-end (&optional context)
  "Assuming we're in an org src block, move point to the end.
CONTEXT is the block info as returned by `org-element-at-point'."
  (unless context
    (setq context (org-element-context)))
  (goto-char (min (org-element-property :end context)
                  (point-max)))
  (skip-chars-backward " \t\n"))

(defun ob-gotour-cancel ()
  "Send kill order for pending REQ.
And append a message in the source block's result area saying as much."
  (interactive)
  (let ((where (or org-babel-current-src-block-location
                   (and (org-in-src-block-p) (point))))
        id body)
    (unless where
      (user-error "Couldn't find source block to cancel"))
    ;; Glean req id from whatever's around in results area
    (org-with-point-at where
      (ob-gotour--goto-src-block-end)
      (cond ((search-forward-regexp org-babel-result-regexp nil t)
             ;; In #+RESULT, the first propertized char is R
             (setq id (get-text-property (+ 2 (match-beginning 0)) 'id)))
            ((search-forward-regexp ob-gotour--pending-text-regexp nil t)
             (setq id (get-text-property (match-beginning 0) 'id)))
            (t (user-error "Failed to find results area"))))
    (when (aio-promise-p id)
      (setq id (aio-wait-for id)))
    (cl-assert (integerp id))
    (let* ((inst (ob-gotour--get-inst))
           (ws (oref inst ws))
           (req (gethash id (oref inst reqs)))
           (payload (list :Id (format "%d" id) :Kind "kill")))
      (unless req
        (user-error "Request %d already cancelled or returned" id))
      (setq body (concat "Cancelled "
                         (and (bound-and-true-p org-time-stamp-formats)
                              (format-time-string (cdr org-time-stamp-formats)
                                                  (current-time)))))
      (setf (ob-gotour--request-finalizer req)
            (lambda (r) (ob-gotour--modify-result r body 'append)))
      (websocket-send-text ws (json-encode-plist payload)))))

(defun ob-gotour--hack-result (append)
  "Maybe clear existing results area and place point on following line.
With APPEND, don't clear beforehand.  Caller is expected to insert
content containing a final newline after this function returns."
  (let* ((elt (org-element-at-point))
         (type (car elt))
         (key (org-element-property :key elt))
         (value (org-element-property :value elt))
         (existingp (not (string-empty-p value)))
         (begin (org-element-property :begin elt))
         (end (org-element-property :end elt))
         (post-blank (org-element-property :post-blank elt))
         (post-affiliated (org-element-property :post-affiliated elt))
         (effective-end (- end post-blank (if (eq type 'paragraph) 1 0))))
    ;; The :post-affiliated field's value is always the first char of :value,
    ;; if nonempty, otherwise looking-at #+RESULT (b-o-l)
    (when (= begin post-affiliated)
      ;; Must be pending or new content
      (cl-assert (string= key org-babel-results-keyword))
      (cl-assert (string= value ""))
      (save-excursion
        (goto-char end)
        (when (looking-at-p ob-gotour--pending-text-regexp)
          (delete-region (point) (min (point-max) (+ 2 (line-end-position)))))))
    (if (and existingp append)
        (goto-char effective-end)
      (when existingp
        ;; Plus one for newline because we expect caller to insert content
        ;; that includes a newline
        (delete-region post-affiliated (min (point-max) (1+ effective-end))))
      (cl-assert (looking-at-p org-babel-result-regexp))
      (end-of-line))
    (forward-char)))

(defun ob-gotour--insert-pending-placeholder (req)
  "Insert `ob-gotour--pending-text' in results area for REQ.
Also add a text property for the request id to the meta line."
  (ob-gotour--with-result-keyword req
    (goto-char (org-babel-where-is-src-block-result 'insert))
    (ob-gotour--hack-result nil)
    (forward-line -1)
    (cl-assert (looking-at-p org-babel-result-regexp))
    (delete-region (point) (line-end-position))
    (insert (ob-gotour--get-pending-text (if (aio-promise-p req)
                                             req
                                           (ob-gotour--request-id req))))))

(defun ob-gotour--remove-inline-images ()
  "Remove inline image overlays from org element at point."
  (when (bound-and-true-p org-inline-image-overlays)
    (let* ((elt (org-element-at-point))
           (ovs (overlays-in (org-element-property :begin elt)
                             (org-element-property :end elt))))
      (dolist (ov ovs)
        (when (memq ov org-inline-image-overlays)
          (delete-overlay ov)
          (setq org-inline-image-overlays
                (delete ov org-inline-image-overlays)))))))

(defun ob-gotour--modify-result (req content &optional append raw)
  "Replace or APPEND CONTENT to existing result area for REQ.
With RAW, don't prepend colons to each line."
  (ob-gotour--with-result-keyword req
    (goto-char (org-babel-where-is-src-block-result 'insert))
    (when raw
      (ob-gotour--remove-inline-images))
    (ob-gotour--hack-result append)
    (unless raw
      (setq content
            (concat ": "
                    (string-join (split-string content "\r?\n" t "[ \r\n]+")
                                 "\n: ")
                    "\n")))
    (insert (string-trim-right content) "\n")))

(cl-defmethod ob-gotour--create-payload ((inst ob-gotour--transport)
                                         (req ob-gotour--request))
  "Prepare payload using REQ and INST.
BODY and PARAMS are added to this object.  Responses are handled by
`ob-gotour--on-message-handler' according to value of the result's
`:Kind' field.  This function does not block."
  (with-slots (id ws reqs) inst
    (cl-incf id)
    (cl-assert (eq 'open (websocket-ready-state ws)))
    (let ((payload (list
                    :Id (format "%d" id)
                    :Kind "run"
                    :Body (ob-gotour--request-body req)
                    :Options nil)))
      (setf (ob-gotour--request-id req) id)
      (puthash id req reqs)
      ;; XXX last call for manipulating params: `org-babel-insert-result'
      ;; runs soon after this returns.
      payload)))

(defvar ob-gotour--pending-tag "pending")

(defmacro ob-gotour--org-get-tags ()
  "Call `org-get-tags' in a backward-compatible way."
  (require 'org)
  (if (= 2 (cdr (func-arity #'org-get-tags)))
      '(org-get-tags nil t)
    '(org-get-tags)))

(defun ob-gotour--add-pending-tag ()
  "Add a tag named pending to the current entry."
  (remove-hook 'org-babel-after-execute-hook #'ob-gotour--add-pending-tag t)
  (save-excursion
    (when (outline-previous-heading)
      (let ((existing (ob-gotour--org-get-tags)))
        (org-set-tags (cons ob-gotour--pending-tag existing))))))

(defun ob-gotour--remove-pending-tag ()
  "Remove a tag named pending from the current entry."
  (save-excursion
    (when (outline-previous-heading)
      (when-let ((existing (ob-gotour--org-get-tags)))
        (org-set-tags (remove ob-gotour--pending-tag existing))))))

(defun org-babel-execute:gotour (body params)
  "Execute BODY given PARAMS."
  (add-hook 'org-babel-after-execute-hook #'ob-gotour--add-pending-tag t t)
  ;;
  ;; XXX help! modifying params must be a terrible hack, right? Couldn't find
  ;; any seams to get at them prior to this point. Surely one or more exist?
  ;;
  (setcdr (cdr (assq :result-params params)) '("silent"))
  ;;
  (let ((req (make-ob-gotour--request
              :marker (copy-marker org-babel-current-src-block-location)
              :params params
              :body body)))
    (ob-gotour--insert-pending-placeholder
     (aio-with-async
       (let ((ws (aio-await (ob-gotour--ensure-connected)))
             (payload (ob-gotour--create-payload (ob-gotour--get-inst) req)))
         (websocket-send-text ws (json-encode-plist payload))
         (ob-gotour--request-id req)))))
  nil)

;;;###autoload
(defun ob-gotour-on-org-mode ()
  "Major mode hook for registering lang with various Babel facilities.

This exists to dissuade users from adding the language immediately after
Org loads.  That's because `org-babel-do-load-languages', calls `require'
on the candidate, which pollutes the global symobls table with this
package's names during all Emacs sessions."
  (unless (assoc 'gotour org-babel-load-languages)
    (when (locate-library "go-mode")
      (with-eval-after-load 'org-tangle
        (add-to-list 'org-babel-tangle-lang-exts '("gotour" . "go")))
      (add-to-list 'org-src-lang-modes '("gotour" . go) t))
    (org-babel-do-load-languages
     'org-babel-load-languages
     (append org-babel-load-languages '((gotour . t))))))

(defun ob-gotour--maybe-fill-paragraph ()
  "Fill paragraph elements and move point.
Return a boolean indicating whether or not to keep going."
  (let* ((elt (org-element-at-point))
         (type (car elt))
         (op (point))
         contents-begin)
    (cond ((eq type 'paragraph)
           (org-fill-element)
           (goto-char (org-element-property :end (org-element-at-point))))
          ((and (setq contents-begin (org-element-property :contents-begin elt))
                (> contents-begin (point)))
           (goto-char contents-begin))
          (t (goto-char (org-element-property :end elt))))
    (< op (point))))

(defun ob-gotour-fill-all ()
  "Apply `org-fill-paragraph' to every paragraph in buffer."
  (interactive)
  (save-excursion
    (save-restriction
      (widen)
      (org-show-all)
      (goto-char (point-min))
      (while (org-before-first-heading-p) (forward-line +1))
      (while (ob-gotour--maybe-fill-paragraph)
        (if (window-system) (sit-for 0)
          (redisplay)
          (sleep-for 0.001))))))

;; For now, this project is NOT using GitLab's "release" endpoints, which are
;; comparable to GitHub's but still a work in progress. Instead, this uses an
;; artifact-specific endpoint that is (also) exposed apart from the REST API.
;; This may be the least stable of the three, the REST API being the most
;; solid because it can't change within the same version. There's no such
;; guarantee for these artifact-specific, "well-known" endpoints.
;;
;; Grabbing the entire zip archive would be more convenient than the
;; current solution, which involves re-archiving everything during a final CI
;; stage. However, it seems Emacs's arc-mode relies on external programs,
;; which would complicate things (true?).

(defvar ob-gotour--assets-filepath "assets.tar.gz"
  "The term filepath is a GitLab release concept.
It refers to the path portion of a release link's target URL.")

;; FIXME use packages API instead
(defvar ob-gotour--assets-url
  (concat "https://gitlab.com/jpneverwas/ob-gotour/-"
          "/jobs/artifacts/" ob-gotour-version
          "/raw/" ob-gotour--assets-filepath
          "?job=release")
  ;; This variable can be let bound to the second of these two v4 REST API
  ;; counterparts for testing or debugging.  For private branches, you may
  ;; need to bind `url-request-extra-headers' to an alist with the cons
  ;; ("Private-Token" . "Abc123").
  ;;
  ;; /api/v4/projects/%d/jobs/artifacts/<rev>/download?job=<job>
  ;; /api/v4/projects/%d/jobs/artifacts/<rev>/raw/<member>?job=<job>
  ;;
  ;; The only downside to using them is that the username and project
  ;; components are replaced by a nonhuman (but at least immutable) project
  ;; ID.  Here, <job> is the name of the pipeline job that produces the
  ;; artifact.  <member> is the path relative to the archive's root and may
  ;; include slashes.  <rev> is a commit, branch, or tag.
  ;;
  ;; Note: currently, the artifact-specific URL below just redirects to the
  ;; same path with an injected job ID component:
  ;;
  ;; /<ns>/<project>/-/jobs/<job-id>/artifacts/raw/<member>
  ;;
  ;; where <ns> is the group/username or just username.
  ;;
  ;; TODO look into whether query must be encoded manually
  "Full URL for fetching an artifact member.")

(aio-defun ob-gotour--fetch-assets (dest &optional url)
  "Fetch predefined archive file from `ob-gotour-asset-channel'.
Save it to archive path DEST, clobbering, if necessary.  Return the
`url-retrieve' buffer."
  (unless url (setq url ob-gotour--assets-url))
  (pcase-let ((`(,status . ,buf) (aio-await (aio-url-retrieve url))))
    (if-let ((redirect (plist-get status :redirect)))
        (aio-await (ob-gotour--fetch-assets dest redirect))
      (when-let ((err (plist-get status :error)))
        (error "Problem downloading %s: %S" dest err))
      (with-current-buffer buf
        (goto-char (point-min))
        (search-forward-regexp
         "^HTTP/[[:digit:].]+ \\([[:digit:]]+\\) [[:alnum:] ]+$" nil t)
        (unless (string= (match-string 1) "200")
          (error "Problem downloading %s: see %S" dest (buffer-name)))
        (search-forward "\n\n")
        (set-buffer-multibyte nil)
        (write-region (point) (point-max) dest)
        (current-buffer)))))

(defun ob-gotour--extract-assets (dest-dir)
  "Extract assets archive contents into DEST-DIR, clobbering as needed."
  (cl-assert (file-exists-p dest-dir))
  (let* ((path (concat "share/" ob-gotour--assets-filepath))
         (archive (expand-file-name path (ob-gotour--cache-dir)))
         (target (expand-file-name ob-gotour--assets-filepath dest-dir)))
    (cl-assert (file-exists-p archive))
    (copy-file archive target)
    (let ((buf (find-file-noselect target)))
      (unwind-protect
          (with-current-buffer buf (tar-untar-buffer))
        (kill-buffer buf)
        (delete-file target)))))

;;;###autoload
(defun ob-gotour-init (dest-dir &optional arg)
  "Populate DEST-DIR with \"A Tour of Go\" as an Org file.
Download it first, if necessary, along with supporting ingredients. With
`\\[universal-argument] \\[ob-gotour-init]' (ARG), pop to a dired buffer
for DEST-DIR instead of opening the `ob-gotour-default-variant'. With
`\\[universal-argument] \\[universal-argument] \\[ob-gotour-init]',
always download and extract the latest version, clobbering any existing
tour files along the way (all without asking).

All but the last component of DEST-DIR must exist."
  (interactive "GWorking dir: \nP")
  (setq dest-dir (file-name-as-directory dest-dir))
  (ob-gotour--ensure-temp-dir)
  (ob-gotour--ensure-cache-dir)
  (unless (file-exists-p dest-dir)
    (mkdir dest-dir))
  (let* ((assets-dir (expand-file-name "share/" (ob-gotour--cache-dir)))
         (prefer-dired (member (car arg) '(4 64)))
         (force (member (car arg) '(16 64)))
         (variant (format "tour-%s.org" ob-gotour-default-variant))
         (target (expand-file-name variant dest-dir))
         (default (expand-file-name "tour.org" dest-dir))
         (archive (concat ob-gotour-version "-" ob-gotour--assets-filepath))
         (abort (and-let*
                    (((not force))
                     (existing (directory-files dest-dir nil "tour.*\\.org"))
                     ((not (yes-or-no-p
                            (format "Files exist: %s\nOverwrite? "
                                    (string-join existing ", "))))))))
         buf)
    ;; Must needs investigate if this is the same thing as `aio-with-async'
    ;; Basically just want to schedule task and return immediately.
    (unless abort
      (funcall
       (aio-lambda ()
         (unless ob-gotour-srcdir
           (unless (ob-gotour--cache-dir-populated-p)
             (aio-await (ob-gotour--populate-cache-dir))))
         (let ((default-directory assets-dir))
           (unless (and (file-exists-p archive) (not force))
             (setq buf (aio-await (ob-gotour--fetch-assets
                                   (expand-file-name archive))))
             (make-symbolic-link archive ob-gotour--assets-filepath 'clobber)
             (when-let ((proc (get-buffer-process buf)))
               (set-process-query-on-exit-flag proc nil))
             (kill-buffer buf)))
         (ob-gotour--extract-assets dest-dir)
         (make-symbolic-link target default t)
         (unless force (require 'org) (ob-gotour-on-org-mode))
         (when buf (kill-buffer buf))
         (if prefer-dired
             (dired dest-dir)
           (find-file default)))))))

(defun ob-gotour--make-fmt-request (body imports)
  "Ask the formatting service to format BODY.
With IMPORTS, fix import definitions as well.  Return a plist with items
:Body and :Error, both strings, with one always empty.  This function
mimics the AngularJS fmt() service, found in static/js/services.js of
the tour repo, and its response hander in static/js/controller.js"
  (let* ((url-request-method "POST")
         (url-request-extra-headers
          '(("Content-Type" . "application/x-www-form-urlencoded")))
         (url-request-data
          (url-build-query-string `(("body" ,body)
                                    ("imports" ,(if imports "true" "false")))))
         (url (format "http://%s/_/fmt" (ob-gotour--get-joined-tcp-addr)))
         (buf (url-retrieve-synchronously url t)))
    (prog1
        (with-current-buffer buf
          (goto-char (point-min))
          (unless (looking-at-p "^HTTP/1\\.1 200" )
            (error "Problem talking to server"))
          (search-forward "\n\n")
          (ob-gotour--json-parse-string (buffer-substring (point) (point-max))))
      (kill-buffer buf))))

(aio-defun ob-gotour--fumpt (ignore-imports)
  (unless (when-let ((inst (ob-gotour--get-inst))
                     (proc (oref inst proc)))
            (process-live-p proc))
    (if (yes-or-no-p "Start server? ")
        (aio-await (ob-gotour--ensure-connected))
      (user-error "%s" "No server found, aborting format request")))
  (let* (editing
         (body (cond ((org-src-edit-buffer-p)
                      (setq editing t) (buffer-string))
                     ((org-in-src-block-p)
                      (org-element-property :value (org-element-at-point)))
                     (t (user-error "Not in src block or src buffer"))))
         (res (ob-gotour--make-fmt-request body (not ignore-imports)))
         (err (plist-get res :Error)))
    (if (string-empty-p err)
        (progn
          (unless editing
            (cl-assert (org-edit-src-code))
            (cl-assert (org-src-edit-buffer-p)))
          (let ((op (point)))
            (widen)
            (delete-region (point-min) (point-max))
            (insert (plist-get res :Body))
            (goto-char (min op (point-max))))
          (unless editing
            (org-src-do-at-code-block
             (org-with-point-at (point)
               (when-let ((resblk (org-babel-where-is-src-block-result nil)))
                 (when (save-excursion
                         (goto-char resblk)
                         (forward-line 1)
                         (looking-at-p "^: Fmt: "))
                   (org-babel-remove-result)))))
            (org-edit-src-exit)))
      (if editing
          (user-error "%s" err)
        (org-with-point-at (or org-babel-current-src-block-location (point))
          (goto-char (org-babel-where-is-src-block-result 'insert))
          (ob-gotour--hack-result nil)
          (insert (propertize (concat ": Fmt: " (string-trim-right err))
                              'type 'ob-gotour-fmt-error)
                  "\n"))))))

(defun ob-gotour-fmt (&optional ignore-imports)
  "Format source block at point or current `org-src-mode' buffer.
With IGNORE-IMPORTS, don't attempt to clean up imports.  Show errors in
the minibuffer or the results area as needed"
  (interactive "P")
  ;; Fire and forget
  (ob-gotour--fumpt ignore-imports))

;; TODO provide interactive command that uses this to create or replace
;; content belonging to playground block at point
(defun ob-gotour--playground-paste-fetch (url)
  "Grab content from shared playground URL.
Warning: this may make an external (internet) request, for example to
play.golang.org."
  (let* ((buf (url-retrieve-synchronously url t)))
    (prog1
        (with-current-buffer buf
          (goto-char (point-min))
          (unless (looking-at-p "^HTTP/1\\.1 200" )
            (error "Problem retrieving playground content"))
          (search-forward "\n\n")
          (let ((parsed (libxml-parse-html-region (point) (point-max))))
            (catch 'textarea
              (dolist (e (cddr (assoc 'body (cddr parsed))))
                (when-let ((found (and (eq (car-safe e) 'div)
                                       (assoc 'textarea (cdr e)))))
                  (throw 'textarea (caddr found)))))))
      (kill-buffer buf))))

;; TODO provide command making use of this when point is in some src block
;; you want to share
(defun ob-gotour--playground-paste-share (url body)
  "Ask a playground remote at URL for a leaf/slug unique to BODY.
The public playground URL is https://play.golang.org/share"
  (let* ((url-request-method "POST")
         (url-request-extra-headers
          '(("Content-Type" . "text/plain; charset=utf-8")))
         (url-request-data body)
         (buf (url-retrieve-synchronously url t)))
    (prog1
        (with-current-buffer buf
          (goto-char (point-min))
          (unless (looking-at-p "^HTTP/1\\.1 200" )
            (error "Problem retrieving playground link"))
          (search-forward "\n\n")
          (concat url "/p/" (buffer-substring (point) (point-max))))
      (kill-buffer buf))))

(provide 'ob-gotour)

;;; ob-gotour.el ends here
