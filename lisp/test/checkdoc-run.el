;;; -*- lexical-binding: t -*-

;; These do the same but w. scraping, perhaps for compat:
;;
;; - https://github.com/Alexander-Miller/treemacs/blob/master/test/checkdock.el
;; - https://github.com/flycheck/flycheck/blob/master/maint/flycheck-checkdoc.el
;;

(require 'checkdoc)
(require 'subr-x)


(defun checkdoc-run--clean (content)
  (let ((parts (split-string content "[\xc]+" t "[ \t\r\n]+")))
    (princ (concat (string-join parts "\n\n") "\n"))))


(defun checkdoc-run--really (file)
  (let* ((buffers (find-file-noselect file nil nil t))
         (checkdoc-diagnostic-buffer "*checkdoc-run*")
         (checkdoc-force-docstrings-flag nil)
         messages ; vs. counter++ (for debugging)
         (checkdoc-create-error-function
          (lambda (&rest r)
            (push r messages)
            (apply #'checkdoc--create-error-for-checkdoc r)))
         (ec 0))
    ;; `find-file-noselect' will pop a singleton, but we need to ensure a list
    (unless (listp buffers) (setq buffers (list buffers)))
    (dolist (buf buffers)
      (with-current-buffer buf
        (checkdoc-current-buffer t)))
    (unless (zerop (setq ec (length messages)))
      (with-current-buffer checkdoc-diagnostic-buffer
        (checkdoc-run--clean (buffer-string))))
    ec))


(defun checkdoc-run (file)
  (condition-case err
      (kill-emacs (checkdoc-run--really file))
    (error err
           (backtrace)
           (signal (car err) (cdr err)))))
