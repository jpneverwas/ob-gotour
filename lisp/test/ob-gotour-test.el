;;; ob-gotour-test.el --- Tests for ob-gotour -*- lexical-binding: t -*-
(require 'ert)
(require 'ob-gotour)

;; Basic dialog:
;;
;; -> {"Id":"42","Kind":"run","Body":"...","Options":null}
;; <- {"Id":"42","Kind":"stdout","Body":"Hello\n"}
;; <- {"Id":"42","Kind":"end","Body":""}
;;
;; Kind.start isn't actually returned from the server
;; despite what playground.js says.
;;

(declare-function org-babel-execute-src-block "ob-core" (&optional arg info params))

(defvar ob-gotour-test-debug (and (getenv "GOTOUR_DEBUG") t))
(defvar ob-gotour-test-tempdir
  (file-name-as-directory
   (or (getenv "OB_GOTOUR_TEST_TEMPDIR")
       (expand-file-name "obtour-test/emacs" temporary-file-directory))))

(defmacro ob-gotour-test-wait (timeout &rest body)
  (declare (indent 1))
  (let ((res (make-symbol "res")))
    `(let (,res)
       (with-timeout (,timeout (error "Failed: %S\n\nOutput:\n%s"
                                      ',body (buffer-substring-no-properties
                                              (point-min) (point-max))))
         (while (not (setq ,res ,(macroexpand-all (cons 'progn body))))
           (sleep-for 0.1))
         ,res))))

(defun ob-gotour-test--ensure-clean(inst)
  (should-not (oref inst ws))
  (should-not (oref inst proc))
  (should (= 0 (oref inst id)))
  (should (= 0 (hash-table-count (oref inst reqs)))))

(ert-deftest ob-gotour-test--meta-wait ()
  (let ((count 0))
    (should-error (ob-gotour-test-wait 1 (cl-incf count) nil))
    (should (= count 10))))

(ert-deftest ob-gotour-test-get-inst ()
  (should-not (eq (ob-gotour--get-inst) (symbol-value 'eieio-unbound)))
  (when-let ((existing (ob-gotour--get-inst)))
    (should (object-of-class-p existing 'ob-gotour--transport)))
  (let ((inst (make-instance 'ob-gotour--transport)))
    (should (eq inst (ob-gotour--get-inst)))))

(defmacro ob-gotour-test-with-instance (inst &rest body)
  "Bind INST's slot names as dollar-prefixed local vars inside BODY."
  (declare (indent defun))
  (unless (and (class-p inst) (object-of-class-p inst 'ob-gotour--transport))
    (push inst body)
    (setq inst nil))
  `(let ($inst)
     (with-slots (($id id)
                  ($proc proc)
                  ($ws ws)
                  ($reqs reqs))
         (setq $inst (or ,inst (make-instance 'ob-gotour--transport)))
       (ignore $inst)
       ,@body)))

(ert-deftest ob-gotour-test--meta--with-instance ()
  (ert-info ("Slots bound to convenience vars")
    (ob-gotour-test-with-instance
      (ob-gotour-test--ensure-clean $inst)
      (let ((req (make-ob-gotour--request
                  :id $id
                  :marker nil
                  :params nil
                  :body "")))
        (puthash $id req $reqs)
        (cl-incf $id)
        ;; Assigning to place works
        (should (eq (oref $inst id) 1))
        ;; Zero still there
        (should (eq (gethash 0 $reqs) req))
        (should-not (zerop (hash-table-count $reqs)))
        (clrhash $reqs)
        (should (zerop (hash-table-count $reqs)))
        (setq $id 0)))))

(ert-deftest ob-gotour-test--meta--with-instance-nested ()
  (ert-info ("Nested call to with-instance fails")
    (let ((existing))
      (ob-gotour-test-with-instance
        (ob-gotour-test--ensure-clean $inst)
        (should (eq (ob-gotour-test-with-instance (setq existing $inst))
                    $inst))
        (should (eq existing $inst))))))

;; This is just for confirming the right JSON API is in effect
(ert-deftest ob-gotour-test--meta-parse-frame ()
  (ert-info ("(meta) parses raw JSON as expected")
    (let* ((raw "{\"Id\":\"1\",\"Kind\":\"stdout\",\"Body\":\"Hello, \u4e16\u754c\\n\"}")
           (result (ob-gotour--json-parse-string raw)))
      (should (equal result '(:Id "1" :Kind "stdout" :Body "Hello, 世界\n"))))))

(defvar ob-gotour-test-starting-port 23999)

;; This was stolen from `lsp--find-available-port'
(defun ob-gotour-test--find-unused-port ()
  (catch 'port
    (let ((port ob-gotour-test-starting-port))
      (while
          (condition-case _err
              (progn
                (delete-process (open-network-stream "*ob-gotour-port-test*"
                                                     nil "127.0.0.1" port
                                                     :type 'plain))
                (cl-incf port))
            (file-error (throw 'port port)))))))

;; For I/O, use `ob-gotour-test-with-cache-dir' below
(defmacro ob-gotour-test-with-tempdir (name &rest body)
  (let ((actual (make-symbol "actual"))
        (well-known (make-symbol "well-known")))
    `(progn
       ;; Sanity first
       (should (file-exists-p ob-gotour-test-tempdir))
       (should (directory-name-p ob-gotour-test-tempdir))
       (should (string-prefix-p temporary-file-directory ob-gotour-test-tempdir))
       (let* ((default-directory ob-gotour-test-tempdir)
              (go-cache (expand-file-name ".go" ob-gotour-test-tempdir))
              (,well-known (expand-file-name ,name))
              (,actual (make-temp-file (concat ,well-known "-") t)))
         (unless (file-exists-p go-cache)
           (make-directory (expand-file-name "src" go-cache) 'parents)
           (make-directory (expand-file-name "pkg" go-cache) 'parents))
         (cl-assert (file-name-absolute-p ,actual))
         (make-symbolic-link (file-name-base ,actual) ,name t)
         (let ((default-directory (file-name-as-directory ,actual)))
           ,@body)))))

(defun ob-gotour-test-dump-buf (buf)
  (with-current-buffer buf
    (message "\n<<<<<<<<<<<<<<<<<<<<<<<< %s\n%s\n"
             buf
             (string-trim (buffer-string)))))

(defmacro ob-gotour-test-with-cache-dir (name &rest body)
  (declare (indent 1))
  `(ob-gotour-test-with-tempdir
    ,name
    (should-not (ob-gotour--cache-dir-populated-p))
    (make-directory "tempdir")
    (let* (ob-gotour--cache-dir
           (ob-gotour-tempdir (expand-file-name "tempdir/"))
           (ob-gotour--skip-temp-dir-hook t)
           (ob-gotour-cache-dir (expand-file-name "cachedir/"))
           (go-cache (expand-file-name ".go" ob-gotour-test-tempdir))
           (sxdir (expand-file-name
                   ob-gotour--pinned-tour-version-suffix
                   (ob-gotour--cache-dir)))
           (inhibit-message (not ob-gotour-test-debug))
           (ob-gotour-tcp-addr (list "127.0.0.1"
                                     (ob-gotour-test--find-unused-port)))
           ob-gotour--cache-dir-sentinel)
      (ob-gotour--ensure-cache-dir)
      (let ((default-directory sxdir))
        (make-symbolic-link (expand-file-name "pkg/" go-cache) "pkg/")
        (make-symbolic-link (expand-file-name "src/" go-cache) "src/"))
      (aio-wait-for (ob-gotour--populate-cache-dir))
      (ert-info ("Wait for other tests")
        (when-let ((inst (ob-gotour--get-inst)))
          (ob-gotour-test-wait 10
            (not (process-live-p (oref inst proc))))))
      ,@body)
    (should-not (ob-gotour--cache-dir-populated-p))))

(ert-deftest ob-gotour--call-go ()
  (ob-gotour-test-with-tempdir
   "call-go"
   (let* ((ob-gotour-tempdir (expand-file-name "tempdir/"))
          proc)
     (mkdir "tempdir")
     (setq proc (aio-wait-for (ob-gotour--call-go "env" "GOMODCACHE")))
     (should (zerop (process-exit-status proc)))
     (should (with-current-buffer "*ob-gotour-setup-stderr*"
               (goto-char (point-min))
               (search-forward "exited" nil t)))
     (should (with-current-buffer "*ob-gotour-setup-stdout*"
               (goto-char (point-min))
               (string-suffix-p "/mod"
                                (buffer-substring (point) (point-at-eol))))))))

(ert-deftest ob-gotour--populate-cache-dir ()
  (ob-gotour-test-with-cache-dir
      "populate-cache-dir"
    (let ((ob-gotour-tempdir (expand-file-name "tempdir/"))
          (ob-gotour-cache-dir (expand-file-name "cachedir/"))
          (inhibit-message (not ob-gotour-test-debug))
          ob-gotour--cache-dir-sentinel)
      (ert-info ("Cache populated from fake project")
        (should (file-exists-p ob-gotour-tempdir))
        (should (file-exists-p (ob-gotour--cache-dir)))
        (should (ob-gotour--cache-dir-populated-p)))
      (when ob-gotour-test-debug
        (when-let ((buf (get-buffer "*ob-gotour-setup-stdout*")))
          (ob-gotour-test-dump-buf buf))
        (when-let ((buf (get-buffer "*ob-gotour-setup-stderr*")))
          (ob-gotour-test-dump-buf buf))))))

(ert-deftest ob-gotour-test-server-baseline ()
  :tags '(io)
  (ob-gotour-test-with-cache-dir
      "server-baseline"
    (let ((inst (make-instance 'ob-gotour--transport)))
      (ert-info ("Server instance created")
        (ob-gotour--start-server inst)
        (should (process-live-p (oref inst proc))))
      (with-temp-buffer
        (let* ((host (car ob-gotour-tcp-addr))
               (port (cadr ob-gotour-tcp-addr))
               (proc (open-network-stream
                      "server-baseline" (current-buffer)
                      host port))
               (req (format "GET /tour/ HTTP/1.1\r\nHost: %s\r\n\r\n" host)))
          (ert-info ("Server serving HTTP")
            (set-process-query-on-exit-flag proc nil)
            (ob-gotour-test-wait 2 (eq 'open (process-status proc)))
            (process-send-string proc req)
            (process-send-eof proc)
            (ob-gotour-test-wait 2 (eq 'closed (process-status proc)))
            (goto-char (point-min))
            (when ob-gotour-test-debug
              (princ (buffer-string)))
            (ob-gotour-test-wait 2 (not (process-live-p proc)))
            (should (search-forward "HTTP/1.1 200 OK"))
            (should (search-forward "<!doctype html>"))
            (should (search-forward "</html>")))))
      (kill-process (oref inst proc))
      (ob-gotour-test-wait 2 (not (process-live-p (oref inst proc)))))))

(defmacro ob-gotour-test-with-buffer (buffer &rest body)
  (declare (indent 1))
  `(ob-gotour-test-with-instance (ob-gotour--get-inst)
     (setq websocket-debug ob-gotour-test-debug)
     (unwind-protect
         (progn ,@body)
       (when ob-gotour-test-debug
         (dolist (b (seq-filter (lambda (b)
                                  (string-prefix-p "*websocket"
                                                   (buffer-name b)))
                                (buffer-list)))
           (ob-gotour-test-dump-buf b))
         (ob-gotour-test-dump-buf (get-buffer ,buffer))))))


(ert-deftest ob-gotour-test-server-connection ()
  :tags '(io)
  (ob-gotour-test-with-cache-dir
      "server-connection"
    (let ((inst (make-instance 'ob-gotour--transport)))
      (ert-info ("Ensure server running")
        (ob-gotour--start-server inst)
        (should (process-live-p (oref inst proc))))
      (let* (closed
             messages
             (on-message (lambda (_websocket frame)
                           (push (websocket-frame-text frame) messages)))
             (on-close (lambda (_websocket) (setq closed t))))
        (ob-gotour-test-with-buffer
            " *ob-gotour-server*"
          (should-not $ws)
          (ert-info ("Connect to server's web socket")
            (aio-wait-for
             (ob-gotour--open-connection #'identity on-message on-close)))
          (ert-info ("Socket exists and state ok")
            (should $ws) ; disappears if closed prematurely
            (when ob-gotour-test-debug
              (message "WS-state: %S" (websocket-ready-state $ws)))
            (ob-gotour-test-wait 5 (and $ws (websocket-openp $ws))))
          (ert-info ("Wait till open")
            (ob-gotour-test-wait 5 (eq 'open (websocket-ready-state $ws)))
            (when ob-gotour-test-debug
              (message "WS-state: %S" (websocket-ready-state $ws)))
            (should-not messages))
          (ert-info ("Run some incomplete go code")
            (websocket-send-text
             $ws "{\"Id\":\"1\",\"Kind\":\"run\",\"Body\":\"package main\\n\"}")
            (ob-gotour-test-wait 5
              (when ob-gotour-test-debug
                (message "messages: %S" (length messages)))
              (= 2 (length messages)))

            (should (string-match-p "end" (car messages)))
            (should (string-match-p "stderr" (cadr messages)))
            (should (string-match-p "undeclared" (cadr messages))))
          (let ((orig $ws))
            (websocket-close $ws)
            (ob-gotour-test-wait 5 (and closed (not $ws)))
            (should-not (websocket-openp orig))
            (delete-process $proc)
            (should-not (process-live-p $proc))
            (setq $proc nil)))))))


(defvar ob-gotour-test--src-1
  "package main
import \"fmt\"
func main() {
\tfmt.Println(\"hw\")
}")

(ert-deftest ob-gotour-test-send ()
  (ob-gotour-test-with-cache-dir "send"
    (ert-info ("Connect and run some code")
      (let (ws body)
        (cl-letf (((symbol-function #'ob-gotour--handle-stdout)
                   (lambda (_ b) (setq body b)))
                  ((symbol-function #'ob-gotour--handle-end)
                   (lambda (_ b) (should (string-empty-p b)))))
          (ert-info ("Wait for connection")
            (setq ws (aio-wait-for (ob-gotour--ensure-connected)))
            (should (websocket-p ws)))
          (should (ob-gotour--cache-dir-populated-p))
          (ob-gotour-test-wait 5 (eq 'open (websocket-ready-state ws)))
          (ert-info ("Call run with src--1")
            (let* ((inst (ob-gotour--get-inst))
                   (oid (oref inst id))
                   (req (make-ob-gotour--request
                         :marker (copy-marker
                                  org-babel-current-src-block-location)
                         :params nil
                         :body ob-gotour-test--src-1))
                   (payload (ob-gotour--create-payload inst req)))
              (websocket-send-text ws (json-encode-plist payload))
              (should (= (1+ oid) (oref inst id)))))
          (ob-gotour-test-with-buffer
              " *ob-gotour-server*"
            (ob-gotour-test-wait 4 (equal body "hw\n"))
            (ob-gotour-shutdown)
            (ob-gotour-test-wait 2 (null $ws))
            (should-not (process-live-p $proc))))))))

(defvar ob-gotour-test--src-2
  "* Test
#+BEGIN_SRC gotour
\tpackage main
\timport \"fmt\"
\timport \"golang.org/x/tour/tree\"
\tfunc main() {
\t\tfmt.Println(tree.Tree{})
\t\tfmt.Println(\"hw\")
\t}
#+END_SRC
")

(eval-when-compile (defvar org-babel-load-languages))
(declare-function org-in-src-block-p "org" (&optional inside))
(declare-function org-fill-element "org" (&optional justify))

(defmacro ob-gotour-test-with-restored-langs (&rest body)
  (let ((orig-org-babel-load-languages (make-symbol "orig-org-babel-load-languages"))
        (orig-org-src-lang-modes (make-symbol "orig-org-src-lang-modes"))
        (orig-org-babel-tangle-lang-exts (make-symbol "orig-org-babel-tangle-lang-exts")))
    `(progn
       (require 'org)
       (should (boundp 'org-babel-load-languages))
       (ob-gotour-on-org-mode)
       (let ((,orig-org-babel-load-languages (append (when (boundp 'org-babel-load-languages)
                                                       org-babel-load-languages)
                                                     nil))
             (,orig-org-src-lang-modes (append org-src-lang-modes nil))
             (,orig-org-babel-tangle-lang-exts (append org-babel-tangle-lang-exts nil)))
         (unwind-protect
             (with-temp-buffer
               ,@body)
           (when (boundp 'org-babel-load-languages)
             (setq org-babel-load-languages ,orig-org-babel-load-languages))
           (setq org-src-lang-modes ,orig-org-src-lang-modes)
           (setq org-babel-tangle-lang-exts ,orig-org-babel-tangle-lang-exts))))))

;; XXX find some way to parametrize go-mode on/off (or maybe randomize?)
(defmacro ob-gotour-test--hack-result-common (&rest body)
  `(ob-gotour-test-with-restored-langs
    (insert ob-gotour-test--src-2)
    (let ((org-startup-folded nil))
      (ignore org-startup-folded)
      (org-mode))
    (font-lock-ensure) ; without this, no text props on inner block content
    (insert "\n* Next section\nfoo...\n")
    (when ob-gotour-test-debug
      (ob-gotour-test-dump-buf (current-buffer)))
    (goto-char (point-min))
    (search-forward "BEGIN_SRC")
    (should (org-in-src-block-p))
    (forward-line 1)
    (should (org-in-src-block-p)) ; see just above
    (should-not (save-excursion (search-forward "RESULTS" nil t)))
    ,@body
    (when ob-gotour-test-debug
      (ob-gotour-test-dump-buf (current-buffer)))))

(ert-deftest ob-gotour-test--goto-src-block-end ()
  (ob-gotour-test--hack-result-common
   (should-not (looking-at-p "^#[+]END_SRC$"))
   (ob-gotour--goto-src-block-end)
   (beginning-of-line)
   (should (looking-at-p "^#[+]END_SRC$"))))

(ert-deftest ob-gotour-test--hack-result-ph ()
  :tags '(noio)
  (ob-gotour-test--hack-result-common
   (ert-info ("Placeholder for outstanding request removed before anything")
     (let ((req (make-ob-gotour--request
                 :id 1
                 :marker (point-marker)
                 :params nil
                 :body "fake")))
       (ob-gotour--insert-pending-placeholder req)
       (when ob-gotour-test-debug
         (ob-gotour-test-dump-buf (current-buffer)))
       (ert-info ("Insert propertize result line with empty area")
         (ob-gotour--with-result-keyword req
           (should (get-text-property 0 'id org-babel-results-keyword))
           (goto-char (org-babel-where-is-src-block-result 'insert))
           (should (looking-at-p "^#[+]RESULTS:$"))
           (should (get-text-property (+ 2 (point)) 'id)))))
     (should-not (looking-at-p "^#[+]RESULTS:$"))
     (ob-gotour--goto-src-block-end)
     (forward-line 2)
     (should (looking-at-p "^#[+]RESULTS:$"))
     (should (get-text-property (+ 2 (point)) 'id))
     (ob-gotour--hack-result t)
     (let ((orig-len (point-max)))
       (should (looking-at-p "^$"))
       (forward-line -1)
       (should (looking-at-p "^#[+]RESULTS:$"))
       (should (= orig-len (point-max)))))))

(ert-deftest ob-gotour-test--hack-result-noex-noap ()
  :tags '(noio)
  (ob-gotour-test--hack-result-common
   (ert-info ("Inserting does nothing when nothing exists")
     (goto-char (org-babel-where-is-src-block-result 'insert))
     (let ((orig-len (point-max)))
       (should (looking-at-p "^#[+]RESULTS:$"))
       (ob-gotour--hack-result nil)
       (should (looking-at-p "^$"))
       (forward-line -1)
       (should (looking-at-p "^#[+]RESULTS:$"))
       (should (= orig-len (point-max)))))))

(ert-deftest ob-gotour-test--hack-result-noex-ap ()
  :tags '(noio)
  (ob-gotour-test--hack-result-common
   (ert-info ("Appending does nothing when nothing exists")
     (goto-char (org-babel-where-is-src-block-result 'insert))
     (let ((orig-len (point-max)))
       (should (looking-at-p "^#[+]RESULTS:$"))
       (ob-gotour--hack-result t)
       (should (looking-at-p "^$"))
       (forward-line -1)
       (should (looking-at-p "^#[+]RESULTS:$"))
       (should (= orig-len (point-max)))))))

(defmacro ob-gotour-test--hack-result-common-ex (append insertion expected)
  (declare (indent 1))
  (let ((existing `(ert-info ("Inject existing content")
                     (should (looking-at-p "^#[+]RESULTS:$"))
                     (save-excursion
                       (forward-line)
                       (insert ,insertion)))))
    `(ob-gotour-test--hack-result-common
      (ert-info ("Existing removed")
        (goto-char (org-babel-where-is-src-block-result 'insert))
        ;; When replacing, we take length after inserting stuff
        ,(when append existing)
        (let ((orig-len (point-max)))
          ,(unless append existing)
          (should (looking-at-p "^#[+]RESULTS:$"))
          (ob-gotour--hack-result ,append)
          (should (looking-at-p "^$"))
          (forward-line -1)
          (should (looking-at-p ,expected))
          (should (= orig-len (point-max))))))))

(ert-deftest ob-gotour-test--hack-result-ex ()
  :tags '(noio)
  (ob-gotour-test--hack-result-common-ex nil
    ": one line\n"
    "^#[+]RESULTS:$"))

(ert-deftest ob-gotour-test--hack-result-ex-ap ()
  :tags '(noio)
  (ob-gotour-test--hack-result-common-ex t
    ": one line\n"
    "^: one line$"))

(ert-deftest ob-gotour-test--hack-result-ex-raw ()
  :tags '(noio)
  (ob-gotour-test--hack-result-common-ex nil
    "raw line\n"
    "^#[+]RESULTS:$"))

(ert-deftest ob-gotour-test--hack-result-ex-ap-raw ()
  :tags '(noio)
  (ob-gotour-test--hack-result-common-ex t
    "raw line\n"
    "^raw line$"))

(ert-deftest ob-gotour-test--hack-result-ex-ap-raw-mul ()
  :tags '(noio)
  (ob-gotour-test--hack-result-common-ex t
    "raw line one\nraw line two\n"
    "^raw line two$"))

(defmacro ob-gotour-test--meta--with-restored-langs (src &rest body)
  (cl-assert (symbolp src) t)
  `(let (orig-langs
         (buf ,(let* ((full (substring-no-properties (symbol-name src)))
                      (parts (split-string full "-"))
                      (first (car (reverse parts))))
                 (format "%s.org" first))))
     (with-current-buffer (get-buffer-create buf)
       (unwind-protect
           (progn
             (require 'org)
             (ert-info ("Registering langs")
               (when (boundp 'org-babel-load-languages)
                 (setq orig-langs (append org-babel-load-languages nil))
                 (ob-gotour-on-org-mode)))
             (insert ,src)
             (org-mode)
             (font-lock-ensure)
             (goto-char (point-min))
             ,@body)
         (when (ob-gotour--get-inst) (ob-gotour-shutdown))
         (when (boundp 'org-babel-load-languages)
           (setq org-babel-load-languages orig-langs))
         (if ob-gotour-test-debug
             (progn
               (write-file buf)
               (ob-gotour-test-dump-buf (current-buffer)))
           (kill-buffer))))))

(ert-deftest ob-gotour-test-execute-simple ()
  (ob-gotour-test-with-cache-dir "execute-simple"
    (ob-gotour-test--meta--with-restored-langs
     ob-gotour-test--src-2
     (ert-info ("Execute source block")
       (should (search-forward "BEGIN_SRC"))
       (should (org-in-src-block-p))
       (should (= 0 (forward-line)))
       (let (org-confirm-babel-evaluate)
         (ignore org-confirm-babel-evaluate)
         (org-babel-execute-src-block))
       (when ob-gotour-test-debug
         (ob-gotour-test-dump-buf (current-buffer)))
       (should (search-forward "END_SRC"))
       (should-not (save-excursion (search-forward "RESULTS" nil t)))
       (save-excursion
         (search-forward "Pending")
         (should (get-text-property (point) 'type))
         (let ((p (get-text-property (point) 'id)))
           (should (aio-promise-p p))
           (should (integerp (aio-wait-for p)))))
       ;; Pending replaced by results
       (ob-gotour-test-wait 5
         (save-excursion (not (search-forward "Pending" nil t))))
       (ob-gotour-test-wait 5
         (save-excursion (search-forward "hw" nil t)))))))

;; FIXME use names instead of numbers for these stupid things
(defvar ob-gotour-test--src-3
  "* Test
#+BEGIN_SRC gotour
\tpackage main
\timport \"fmt\"
\timport \"time\"
\tfunc main() {
\t\tfor i := 0; i < 5; i++ {
\t\t\ttime.Sleep(100 * time.Millisecond)
\t\t\tfmt.Println(i, \"ok\")
\t\t}
\t}
#+END_SRC
")

(ert-deftest ob-gotour-test-execute-cancel ()
  (ob-gotour-test-with-cache-dir "execute-cancel"
    (ob-gotour-test--meta--with-restored-langs
     ob-gotour-test--src-3
     (let (req)
       (ert-info ("Execute source block")
         (should (search-forward "BEGIN_SRC"))
         (should (org-in-src-block-p))
         (should (= 0 (forward-line)))
         (let (org-confirm-babel-evaluate)
           (ignore org-confirm-babel-evaluate)
           (let ((inhibit-message (not ob-gotour-test-debug)))
             (org-babel-execute-src-block)))
         (ob-gotour-test-wait 5
           (save-excursion (search-forward "Pending" nil t)))
         (let ((id (get-text-property (match-beginning 0) 'id)))
           (should (aio-promise-p id))
           (aio-wait-for id))
         (when ob-gotour-test-debug
           (ob-gotour-test-dump-buf (current-buffer)))
         (ob-gotour--goto-src-block-end))
       (ob-gotour-test-with-instance
         (should-not (zerop (hash-table-count $reqs)))
         (setq req (gethash $id $reqs)))
       (should req)
       (ert-info ("Wait for RESULTS")
         (should-not (save-excursion (search-forward "RESULTS" nil t)))
         (ob-gotour-test-wait 5
           (save-excursion (search-forward "RESULTS" nil t)))
         (save-excursion (search-forward "RESULTS" nil t)
                         (goto-char (match-beginning 0))
                         (should (looking-at-p "RESULTS"))
                         (should (eq (get-text-property (point) 'id)
                                     (ob-gotour--request-id req)))))
       (ert-info ("Ensure line printed")
         (ob-gotour-test-wait 2
           (save-excursion (search-forward "1 ok" nil t))))
       (ert-info ("Fire cancel command")
         (forward-line -1)
         (should (org-in-src-block-p))
         (ob-gotour-cancel)))
     (ert-info ("Finalizer hook ran")
       (ob-gotour-test-wait 2
         (save-excursion (search-forward "Cancelled" nil t))))
     (ert-info ("Default end handler :after also ran")
       (ob-gotour-test-with-instance
         (should (zerop (hash-table-count $reqs)))
         (should-not (gethash $id $reqs))))
     (ert-info ("Never made it to line 3")
       (should-not (save-excursion (search-forward "3 ok" nil t)))))))

(ert-deftest ob-gotour-test-format-error ()
  (ob-gotour-test-with-cache-dir "format-error"
    (ob-gotour-test--meta--with-restored-langs
     ob-gotour-test--src-2
     (require 'go-mode)
     (ert-info ("Start server")
       (aio-wait-for (ob-gotour--ensure-connected)))
     (ert-info ("Call format service")
       (should (search-forward "main()"))
       (delete-char 1)
       (insert "\n\t")
       (ob-gotour-fmt))
     (ert-info ("Error msg inserted into results area")
       (when ob-gotour-test-debug
         (ob-gotour-test-dump-buf (current-buffer)))
       (should (search-forward "END_SRC"))
       (should (save-excursion (search-forward "RESULTS" nil t)))
       (save-excursion
         (search-forward "Fmt:")
         (should (eq 'ob-gotour-fmt-error (get-text-property (point) 'type)))
         (forward-line 1)
         (should (looking-at-p "^$"))))
     (ert-info ("Fix error manually")
       (should (search-backward "main()"))
       (save-excursion (goto-char (match-end 0)) (insert " "))
       (goto-char (line-end-position))
       (delete-region (line-end-position)
                      (and (search-forward "{") (match-beginning 0)))
       (when ob-gotour-test-debug
         (ob-gotour-test-dump-buf (current-buffer))))
     (ert-info ("Call service again")
       (ob-gotour-fmt)
       (should (search-forward "END_SRC"))
       (should-not (search-forward "RESULT" nil t))
       (should-not (search-forward "Fmt:" nil t))))))

;; This obviously can't be called inside a tempdir
(defun ob-gotour-test-last-tagged-url ()
  (should (executable-find "git"))
  (let* ((last (with-temp-buffer
                 (should (zerop (call-process "git" nil t nil
                                              "describe" "--abbrev=0")))
                 (string-trim-right (buffer-string)))))
    (should (eq ?v (aref last 0)))
    (replace-regexp-in-string (regexp-quote ob-gotour-version)
                              last ob-gotour--assets-url)))

(ert-deftest ob-gotour-test-release-endpoint ()
  :tags '(external-io)
  (let ((ob-gotour--assets-url (ob-gotour-test-last-tagged-url)))
    (ob-gotour-test-with-cache-dir
        "release-endpoint"
      (let* ((assets-dir (expand-file-name "share/" (ob-gotour--cache-dir)))
             (work-dir (expand-file-name "work/" (ob-gotour--cache-dir)))
             (archive (expand-file-name "assets.tar.gz" assets-dir)))
        (ert-info ("Download assets")
          (should (file-exists-p assets-dir))
          (should-not (file-exists-p archive))
          (aio-wait-for (ob-gotour--fetch-assets archive))
          (should (file-exists-p archive)))
        (ert-info ("Extract assets")
          (make-directory work-dir)
          (ob-gotour--extract-assets work-dir)
          (should (file-exists-p (expand-file-name "tour-filled.org" work-dir))))))))

(provide 'ob-gotour-test)
;;; ob-gotour-test.el ends here
