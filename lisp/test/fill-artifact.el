;; -*- lexical-binding: t -*-

;; Nest process in a pseudo terminal because CI runners may not provide one
(require 'term)
(require 'subr-x)
(require 'cl-macs)

(defvar max-wait 10.0)
(defconst this-file load-file-name)
(defconst parent-dir
  (file-name-as-directory (expand-file-name ".." (file-name-directory this-file))))
(cl-assert (string-suffix-p "lisp/" parent-dir) t)


(defun do--run()
  (add-hook 'term-mode-hook (lambda () (term-reset-size 20 120)))
  (let* ((emacs (or (getenv "EMACS") "emacs"))
         (src '(progn
                 (require 'ob-gotour)
                 (require 'org)
                 (let ((org-startup-folded nil)) (find-file "tour.org"))
                 (run-with-idle-timer 0 nil (lambda () (let ((fill-column 78))
                                                         (ob-gotour-fill-all)
                                                         (save-buffer)
                                                         (kill-emacs))))))
         (args (list "-Q" "-nw" "-L" parent-dir
                     "-l" this-file
                     "--eval" (format "%S" src)))
         (buf (apply #'make-term "fill-job" emacs nil args))
         (proc (get-buffer-process buf)))
    (sleep-for max-wait)
    (if (process-live-p proc)
        (progn
          (set-process-query-on-exit-flag proc nil)
          (kill-process proc)
          1)
      0)))

(defun do-run ()
  (condition-case err
      (kill-emacs (do--run))
    (error (let ((backtrace-line-length 0))
             (ignore backtrace-line-length)
             (backtrace))
           (signal (car err) (cdr err)))))
