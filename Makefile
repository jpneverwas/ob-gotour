EMACS ?= emacs
CASK ?= cask
GO ?= go
TMPDIR ?= /tmp

# This variable is used for cache breaking in CI script
GO_VERSION = 1.18
GO_TEST_TMPDIR = $(TMPDIR)/obtour-test
IMAGE_DIR = .ob-gotour-images
TAG ?= -t !skip

export OB_GOTOUR_TEST_TEMPDIR = $(GO_TEST_TMPDIR)/emacs

.PHONY: test clean-tour clean build fill

build: tour.org $(IMAGE_DIR)/tree.png $(IMAGE_DIR)/gopher.png

tour.org: $(GO_TEST_TMPDIR)/ObMain/content.out
	cp -v $< $@

$(IMAGE_DIR)/%.png: $(GO_TEST_TMPDIR)/images/%.png | $(IMAGE_DIR)
	@cp -v $^ $@

$(GO_TEST_TMPDIR)/ObMain/content.out: | $(GO_TEST_TMPDIR)/ObMain
$(GO_TEST_TMPDIR)/images/tree.png: | $(GO_TEST_TMPDIR)/images
$(GO_TEST_TMPDIR)/images/gopher.png: | $(GO_TEST_TMPDIR)/images

$(GO_TEST_TMPDIR)/ObMain $(GO_TEST_TMPDIR)/images:
	cd src/obtour && $(GO) test

$(IMAGE_DIR):
	mkdir $@

# Don't depend on tour.org directly to allow for missing dependencies
fill:
	test -f tour.org
	test ! -f tour.org~
	$(CASK) exec $(EMACS) -Q --batch -L lisp \
		-l lisp/test/fill-artifact.el \
		--eval "(progn (setq enable-dir-local-variables nil) (do-run))"

test: | $(OB_GOTOUR_TEST_TEMPDIR)
	$(CASK) exec ert-runner $(PAT) $(TAG) lisp

checkdoc:
	$(CASK) exec $(EMACS) -Q --batch -l lisp/test/checkdoc-run.el \
		--eval "(checkdoc-run \"lisp/*.el\")"

clean-tour:
	@rm -vf $(GO_TEST_TMPDIR)/ObMain

clean:
	@rm -vf lisp/*.elc
	@rm -vf tour.org tour.org~
	@rm -vf ert-profile*
	@rm -vrf $(GO_TEST_TMPDIR)
	@rm -vrf $(IMAGE_DIR)

$(OB_GOTOUR_TEST_TEMPDIR):
	mkdir -p $(OB_GOTOUR_TEST_TEMPDIR)
